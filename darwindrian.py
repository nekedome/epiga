from http.server import HTTPServer
from importlib import util
from multiprocessing import Process, Manager
from socketserver import ThreadingMixIn
from time import sleep
from webui import DarwindrianRequestHandler
import argparse
import os
import webbrowser


class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """ This allows the HTTP server to handle multiple, simultaneous requests """
    pass


def launch_experiment(url, port):
    # Construct the web server listing on url:port
    server_address = (url, port)
    httpd = ThreadedHTTPServer(server_address, DarwindrianRequestHandler)

    # Create shared memory to allow the state of the experiment to persist across pages
    manager = Manager()
    httpd.state = manager.dict()

    # Launch web server
    print("Web server: launching...")
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()  # on interrupt (CTRL^C) kill web server


def main(port, nobroswer):
    """ Spawn a local web server running Darwindrian and open a web browser on its homepage

        The web server will listen to all network connection requests on the given port.

        The default behaviour is to also launch the default local web browser to open the web application, this can be
        disabled by setting nobrowser to True.
    """

    # Check software dependencies
    if util.find_spec("matplotlib") is None:
        print("Warning: matplotlib not installed - some functions disabled.")
    if util.find_spec("PIL") is None:
        print("Error: pillow not installed - cannot render Mondrians.")
        exit(1)
    if not os.path.isfile("genotype-to-phenotype/genotype_to_phenotype.exe"):
        print("Error: genotype_to_phenotype.exe doesn't exist, please build it.")
        exit(1)

    # Address parameters for the web server
    url_server = "0.0.0.0"  # listen for network based connections
    url_client = "localhost"

    # Spawn the web server
    httpd = Process(target=launch_experiment, args=(url_server, port))
    httpd.start()
    sleep(2)  # sleep, allowing the web server to fully start

    if not nobroswer:
        webbrowser.open("http://%s:%u" % (url_client, port))  # launch web browser and open Darwindrian

    # Wait for web server to close
    try:
        httpd.join()
    except KeyboardInterrupt:
        print("Web server: terminated")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--port", type=int, default=8080, help="web server port (default 8080)")
    parser.add_argument("--nobrowser", action="store_true", help="disable launch of local web browser")
    args = parser.parse_args()
    main(args.port, args.nobrowser)
