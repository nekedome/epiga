from ga import GeneticAlgorithm, Optimisation, Model
from gene import BoundedInt, ProbabilityDistribution, Point
from genotype import Genotype
from mondrian import MondrianPhenotype, genotype_to_phenotype
from evolution import Evolution
from selection import Selection
from crossover import Crossover
from mutation import Mutation
from enum import Enum
from util import fitness_proportionate_selection, luck
import copy
import logging
import math
import random


class GeneralEvolution(Evolution):
    """ The general genetic algorithm utilising selection, crossover and mutation """

    def __init__(self, **kwargs):
        super(GeneralEvolution, self).__init__(**kwargs)
        self.required_parameters += ["selection", "crossover", "mutation"]

    def operate_logic(self, individuals, **kwargs):
        """ Core implementation of the GeneralEvolution logic """

        # Attempt to extract phenotypes parameter from keyword arguments
        phenotypes = None
        for key, value in kwargs.items():
            if key == "phenotypes":
                phenotypes = value

        # Start the next generation with the requisite number of elites (i.e. pick individuals with the highest fitness)
        next_generation = []
        assert self.n_elites <= self.population_size
        scores = {individual: individual.fitness(phenotypes=phenotypes) for individual in individuals}
        elites = sorted(scores, key=lambda i: scores[i], reverse=True)  # descending order (use self.optimisation)
        elites = elites[:self.n_elites]  # just get the top n (self.n_elites)
        next_generation += [individual.copy_elite() for individual in elites]  # perform a deep-copy

        # Complete the rest of the next generation
        while len(next_generation) < self.population_size:
            # Perform genetic operators to select parents and derive offspring
            parents = self.selection.operate(individuals, phenotypes=phenotypes)
            offspring = self.crossover.operate(parents)
            offspring = self.mutation.operate(offspring)

            # Truncate offspring if necessary
            n_needed = self.population_size - len(next_generation)
            offspring = offspring[:n_needed]

            # Include offspring into the next generation
            next_generation += offspring

        return next_generation


class RouletteSelection(Selection):
    """ Roulette selection, also known as fitness proportionate selection

        This selection process selects the requisite number of parents to produce offspring by randomly selecting
        individuals without replacement. The probability of selecting any individual is proportional to their fitness
        value; thus given fitter individuals greater probability of passing on their genetic material.
    """

    def operate_logic(self, individuals, **kwargs):
        """ Core implementation of the RouletteSelection logic """

        # Attempt to extract phenotypes parameter from keyword arguments
        phenotypes = None
        for key, value in kwargs.items():
            if key == "phenotypes":
                phenotypes = value

        candidate_parents = individuals  # used to ensure parents are selected without replacement
        parents = []
        scores = {individual: individual.fitness(phenotypes=phenotypes) for individual in individuals}
        logging.debug("scores=%s", scores)

        # Select parents without replacement (i.e. we don't want the same individual used for more than one parent)
        while len(parents) < self.n_parents:
            min_score = min([scores[individual] for individual in candidate_parents])
            total_score = sum([scores[individual] for individual in candidate_parents])

            if total_score == len(candidate_parents) * min_score:
                # All individuals have the same fitness, select uniformly
                new_parent = random.choice(candidate_parents)
            else:
                # Not all individuals have the same fitness, select with probability proportional to fitness
                # The fitness_proportionate_selection function requires non-negative values, rescale them first
                denominator = total_score - len(candidate_parents) * min_score
                scaled_scores = {individual: float(scores[individual] - min_score) / denominator for individual in
                                 candidate_parents}
                logging.debug("scaled_scores=%s", scaled_scores)
                new_parent = fitness_proportionate_selection(scaled_scores)

            logging.debug("new_parent=%s", new_parent)
            parents.append(new_parent)
            candidate_parents.remove(new_parent)

        return parents


class TwoParentSinglePointCrossover(Crossover):
    """ Single point crossover between two parents

        Given the two parents, their respective chromosomes are crossover at a single gene selected uniformly at random.
        This will result in two new chromosomes, each having some (completely complementary) genetic material from both
        parents. These two new chromosomes are the resultant offspring from this crossover operation.
    """

    N_PARENTS = 2  # this crossover method always has two parents

    def __init__(self, **kwargs):
        """ Overload the Crossover.__init__(**kwargs) constructor to enforce this method expecting two parents """
        self.n_parents = TwoParentSinglePointCrossover.N_PARENTS
        self.required_parameters = ["n_parents"]
        super(TwoParentSinglePointCrossover, self).__init__(**kwargs)

    def operate_logic(self, individuals, **kwargs):
        """ Core implementation of the TwoParentSinglePointCrossover logic """

        err_msg = "Class %s requires n_parents==2, but n_parents==%u" % (type(self).__name__, len(individuals))
        assert len(individuals) == self.n_parents, err_msg

        # Identify crossover point randomly
        crossover_point = random.randint(0, individuals[0].n_genes - 1)
        logging.debug("crossover_point=%u", crossover_point)

        # Perform crossover with parents' chromosomes
        parameters = individuals[0].parameters
        chromosome = [individuals[0].chromosome[:crossover_point] + individuals[1].chromosome[crossover_point:],
                      individuals[1].chromosome[:crossover_point] + individuals[0].chromosome[crossover_point:]]

        # Construct new individuals of the same type as the parents
        geno_type = type(individuals[0])
        children = [geno_type(**parameters), geno_type(**parameters)]
        children[0].chromosome = copy.deepcopy(chromosome[0])
        children[1].chromosome = copy.deepcopy(chromosome[1])
        logging.debug("children=%s", children)

        return children


class ConstantMutation(Mutation):
    """ Constant probability mutation of an individual

        Given an individual, probabilistically mutate its genes. Each gene is given the same probability of mutating and
        each mutation happens independently of all others. That is, depending on the probability parameter and chance,
        any number of genes may mutate (0, 1 or even all of them).

        It is fairly standard to target an expectation that 1 gene will mutate in an individual. To achieve this
        behaviour, simply set the probability parameter 'p' to the inverse of the number of genes.
            genotype = SOME_GENOTYPE
            p_val = 1.0 / genotype.n_genes
            mutator = ConstantMutation(p=p_val)
    """

    def __init__(self, **kwargs):
        self.required_parameters = ["p"]
        super(ConstantMutation, self).__init__(**kwargs)

    def operate_logic(self, individuals, **kwargs):
        """ Core implementation of ConstantMutation logic """
        result = []
        for individual in individuals:
            for gene in individual.chromosome:
                if luck(self.p):
                    gene.mutate()
            result.append(individual)
        return result


class UnbiasedDarwindrianGenotype(Genotype):
    """ Mondrian genotype implementation for the "unbiased Darwindrian" implementation

        A genotype's chromosome is defined by:
        - a_min_width: the minimum width of a rectangle to colour
        - a_min_height: the minimum height of a rectangle to colour
        - a_max_width: the maximum width of a rectangle to colour
        - a_max_height: the maximum height of a rectangle to colour
        - c_max: the maximum number of rectangles to colour
        - d: the line direction probability distribution
        - c: the colour selection probability distribution
        - {q_i}: a list of generating/origin points

        This class is responsible for managing objects encapsulating this chromosome structure and for expressing
        phenotypes as needed by other genetic operators.
    """

    N_GENES_LESS_POINTS = 7  # number of genes in chromosome (excluding the variable number of generating points)
    MAX_C_MAX = 3  # maximum value for c_max (the maximum number of rectangles to colour when drawing artworks

    def __init__(self, **kwargs):
        """ Extract experimental hyper-parameters from the keyword-argument list and validate parameters """
        self.required_parameters = ["width", "height", "n_points", "n_loops", "show_points", "exp_path"]
        self.expressed = False
        super(UnbiasedDarwindrianGenotype, self).__init__(**kwargs)
        assert isinstance(self.width, int), "canvas 'width' parameter must be an integer"
        assert isinstance(self.height, int), "canvas 'height' parameter must be an integer"
        assert isinstance(self.n_points, int), "number of points 'n_points' parameter must be an integer"
        assert isinstance(self.n_loops, int), "number of loops 'n_loops' parameter must be an integer"
        assert isinstance(self.show_points, bool), "'show_points' parameter must be boolean"
        assert self.width > 0, "canvas 'width' parameter must be positive"
        assert self.height > 0, "canvas 'height' parameter must be positive"
        assert self.n_points > 0, "number of points 'n_points' parameter must be positive"
        assert self.n_loops > 0, "number of loops 'n_loops' parameter must be positive"
        assert self.exp_path is not None, "experimental archive direction 'exp_path' is missing"
        self._n_genes = self.N_GENES_LESS_POINTS + self.n_points  # account for variable number of generating points

    def key_fields(self):
        """ The key fields of a MondrianGenotype that determine equivalence

            That is, if two MondrianGenotype instances have matching key fields, they are considered equivalent.
        """
        result = (self.a_min_width, self.a_min_height, self.a_max_width, self.a_max_height, self.c_max)
        result += tuple(q_i for q_i in self.q)
        return result

    @property
    def chromosome(self):
        """ Getter for the genotype's chromosome """
        super(UnbiasedDarwindrianGenotype, self).chromosome  # assert self.has_value
        result = [self.a_min_width, self.a_min_height, self.a_max_width, self.a_max_height, self.c_max, self.d, self.c]
        result += self.q
        return result

    @chromosome.setter
    def chromosome(self, value):
        """ Setter for the genotype's chromosome """

        # Some basic validation of the provided chromosome
        assert isinstance(value, list), "provided chromosome must be a list, not a(n) %s" % type(value).__name__
        err_msg = "provided chromosome must have length %u, not %u" % (self._n_genes, len(value))
        assert len(value) == self._n_genes, err_msg

        # Extract and validate the individual genes
        a_min_width = value[0]
        a_min_height = value[1]
        a_max_width = value[2]
        a_max_height = value[3]
        c_max = value[4]
        d = value[5]
        c = value[6]
        q = value[7:7+self.n_points]
        assert isinstance(a_min_width, BoundedInt)
        assert isinstance(a_min_height, BoundedInt)
        assert isinstance(a_max_width, BoundedInt)
        assert isinstance(a_max_height, BoundedInt)
        assert isinstance(c_max, BoundedInt)
        assert isinstance(d, ProbabilityDistribution)
        assert isinstance(c, ProbabilityDistribution)
        assert isinstance(q, list)
        assert len(q) == self.n_points
        for q_i in q:
            assert isinstance(q_i, Point)
            assert q_i.x_lower == self.POINT_DISTANCE_THRESHOLD
            assert q_i.x_upper == self.width - 1 - self.POINT_DISTANCE_THRESHOLD
            assert q_i.x_step == MondrianPhenotype.STROKE_WIDTH
            assert q_i.y_lower == self.POINT_DISTANCE_THRESHOLD
            assert q_i.y_upper == self.height - 1 - self.POINT_DISTANCE_THRESHOLD
            assert q_i.y_step == MondrianPhenotype.STROKE_WIDTH
        assert 0 <= a_min_width.allele <= a_max_width.allele <= self.width
        assert 0 <= a_min_height.allele <= a_max_height.allele <= self.height
        assert 0 <= c_max.allele <= self.MAX_C_MAX

        # If we got this far, all genes have been successfully validated - overwrite the current chromosome
        self.a_min_width = a_min_width
        self.a_min_height = a_min_height
        self.a_max_width = a_max_width
        self.a_max_height = a_max_height
        self.c_max = c_max
        self.d = d
        self.c = c
        self.q = q
        self.has_value = True

    def factory(self):
        """ A factory function to generate a random UnbiasedDarwindrianGenotype instance

            The current object's hyper-parameters are used to initialise the new object, however, the chromosome of the
            newly created object is entirely random and independent from the current object's chromosome.
        """
        retval = type(self)(**self.parameters)  # create a new object with all of the same parameters as self
        retval.a_min_width = BoundedInt(lower=0, upper=self.width // 2)
        retval.a_min_height = BoundedInt(lower=0, upper=self.height // 2)
        retval.a_max_width = BoundedInt(lower=self.width // 2, upper=self.width)
        retval.a_max_height = BoundedInt(lower=self.height // 2, upper=self.height)
        retval.c_max = BoundedInt(lower=0, upper=self.MAX_C_MAX)
        retval.d = ProbabilityDistribution(events=MondrianPhenotype.DIRECTIONS)
        retval.c = ProbabilityDistribution(events=MondrianPhenotype.COLOURS)
        retval.q = None
        x_vals = [self.POINT_DISTANCE_THRESHOLD, self.width - 1 - self.POINT_DISTANCE_THRESHOLD,
                  MondrianPhenotype.STROKE_WIDTH]
        y_vals = [self.POINT_DISTANCE_THRESHOLD, self.height - 1 - self.POINT_DISTANCE_THRESHOLD,
                  MondrianPhenotype.STROKE_WIDTH]
        while retval.q is None or not self.valid_points(retval.q):
            retval.q = [Point(x_lower=x_vals[0], x_upper=x_vals[1], x_step=x_vals[2],
                              y_lower=y_vals[0], y_upper=y_vals[1], y_step=y_vals[2]) for q_i in range(self.n_points)]
        retval.has_value = True
        return retval

    def express(self, name):
        """ Perform phenotype expression """

        # Some basic pre-condition checks
        assert self.has_value, "a genotype cannot express a phenotype without a chromosome set"
        if self.phenotype is not None:
            return  # this genotype has already expressed a phenotype, maintain it

        # Perform the phenotype expression
        attributes = ["a_min_width", "a_min_height", "a_max_width", "a_max_height", "c_max", "c"]
        parameters = self.parameters
        parameters.update({attr: getattr(self, attr) for attr in attributes})
        phenotype = UnbiasedDarwindrianPhenotype(name=name, **parameters)
        self.phenotype = genotype_to_phenotype(self, phenotype)


class UnbiasedDarwindrianPhenotype(MondrianPhenotype):
    """ Mondrian phenotype implementation for the "unbiased Darwindrian" implementation """

    # Configure the presentation of phenotype rating options in the web interface
    RATINGS = Enum("RATINGS", "strongly_like like indifferent dislike strongly_dislike")
    DEFAULT_RATING = RATINGS.indifferent
    POSITIVE_RATINGS = [RATINGS.strongly_like.name, RATINGS.like.name]
    NEGATIVE_RATINGS = [RATINGS.strongly_dislike.name, RATINGS.dislike.name]
    RATINGS_LABEL = "Rating"

    MAX_FITNESS = 2.0  # RATINGS.strongly_like

    def eligible_rectangle(self, h1, h2, v1, v2):
        """ Criteria that rectangles must satisfy to potentially be coloured
            1. the width of a potential rectangle must lie in [a_min_width, a_max_width]
            2. the height of a potential rectangle must lie in [a_min_height, a_max_height]
        """
        criterion_on_x = self.a_min_width <= math.fabs(v1[0][0] - v2[0][0]) <= self.a_max_width
        criterion_on_y = self.a_min_height <= math.fabs(h1[0][1] - h2[0][1]) <= self.a_max_height
        return criterion_on_x and criterion_on_y

    def fitness(self, **kwargs):
        """ The fitness function on phenotypes """
        rating_map = {self.RATINGS.strongly_dislike: -2, self.RATINGS.dislike: -1, self.RATINGS.indifferent: 0,
                      self.RATINGS.like: 1, self.RATINGS.strongly_like: 2}
        return rating_map[self.RATINGS[self.rating]]


def construct_genetic_algorithm(model, width, height, n_points, n_loops, show_points, population_size, n_elites,
                                max_generations, exp_path, weight_k1, weight_k2, n_cores):
    """ Construct a GeneticAlgorithm instance implementing the "unbiased Darwindrian" implementation """
    exemplar = UnbiasedDarwindrianGenotype(model=Model[model], width=width, height=height, n_points=n_points,
                                           n_loops=n_loops, show_points=show_points, exp_path=exp_path,
                                           weight_k1=weight_k1, weight_k2=weight_k2)
    mutation_rate = 1.0 / exemplar.n_genes
    evolution = GeneralEvolution(population_size=population_size, n_elites=n_elites)
    selection = RouletteSelection()
    crossover = TwoParentSinglePointCrossover()
    mutation = ConstantMutation(p=mutation_rate)
    ga = GeneticAlgorithm(model=Model[model], optimisation=Optimisation.MAX,
                          max_generations=max_generations)
    selection.set_parameter(n_parents=crossover.n_parents)
    ga.set_algorithm(evolution)
    ga.set_algorithm(selection)
    ga.set_algorithm(crossover)
    ga.set_algorithm(mutation)
    ga.set_parameter(exemplar=exemplar)
    ga.set_parameter(exp_path=exp_path)
    ga.set_parameter(n_cores=n_cores)
    return ga
