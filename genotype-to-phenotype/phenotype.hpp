#ifndef __PHENOTYPE_H__
#define __PHENOTYPE_H__

#include <set>
#include <vector>

#define N_DISTANCES (4)

/* Type definitions to make code using Standard Template Library clearer */
typedef std::pair<int, int> Point;
typedef std::vector<Point> PointList;
typedef std::vector<int> IndexList;
typedef std::vector<IndexList> LoopedIndexList;
typedef std::pair<Point, Point> Line;

/* Typedef for compass directions to allow easy enumeration */
typedef enum
{
    NORTH = 0,
    SOUTH = 1,
    EAST = 2,
    WEST = 3,
    LAST = 4 /* enumerate from [NORTH,LAST) */
} Direction;

/* Forward declaration for post-fix increment of a Direction */
Direction operator++(Direction & direction, int);

/* Encapsulation of a Phenotype */
class Phenotype
{
    public:
        /* Standard object-oriented functions: create/copy/delete/compare Phenotype objects */
        Phenotype(int width, int height, int n_loops, PointList points, std::vector<double> direction_probability);
        Phenotype(const Phenotype & instance);
        ~Phenotype();
        bool operator<(const Phenotype & other) const;
        bool operator==(const Phenotype & other) const;

        /* Phenotype-specific functions */
        std::set<Line> horizontal() const;
        std::set<Line> vertical() const;
        double p_val();
        void set_p_val(double p_val);
        void accumulate_p_val_mul(double p_val);
        void accumulate_p_val_add(double p_val);
        std::vector<Phenotype> derive_phenotypes(int point_index, double threshold);
        std::vector<Phenotype> derive_valid_phenotypes();
        double emission_probability(int point_index);
        double direction_probability(int point_index, Direction direction);
        void set_step_size(int step_size);
        void merge_lines();
        void describe();

    private:
        /* Private helper functions */
        void remove_direction(int point_index, Direction direction);
        std::set<Point> find_line_ends(Point origin, Direction direction);
        void add_line(int point_index, Point Q, Direction direction, double threshold);
        std::set<Line> merge_lines_horizontal(std::set<Line> lines) const;
        std::set<Line> merge_lines_vertical(std::set<Line> lines) const;
        bool invalid_point(int point_index);
        int snap_to_grid(int value) const;

        /* Member attributes */
        int _width;
        int _height;
        int _n_loops;
        int _n_points;
        PointList _points;
        std::vector<double> _direction_probability;
        std::set<Line> _horizontal;
        std::set<Line> _vertical;
        std::vector<bool> _available[N_DISTANCES];
        double _p_val;
        int _step_size;
};

#endif
