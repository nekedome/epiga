from abc import ABCMeta, abstractmethod
from ga import Model
from mondrian import MondrianPhenotype
from template import GADescription
import copy
import itertools
import math


class Genotype(GADescription):
    """ Abstract Base Class (ABC) for genotypes """

    __metaclass__ = ABCMeta

    POINT_DISTANCE_THRESHOLD = 2 * MondrianPhenotype.STROKE_WIDTH  # the minimum separation between lines and boundaries

    def __init__(self, **kwargs):
        """ Keyword argument aware constructor """
        self.has_value = False
        self.cached_fitness_value = None
        self.phenotype = None
        self.reachable_phenotypes = None
        self.n_epiga_hits = 0
        self.weight_k1 = None
        self.weight_k2 = None
        self.required_parameters += ["model"]
        super(Genotype, self).__init__(**kwargs)

    def __repr__(self):
        """ Simple pretty print function that prints the type of genotype and chromosome value (if set) """
        parent = super(Genotype, self).__repr__()
        try:
            pretty_print = "%s(%s)" % (parent, self.chromosome)
        except ValueError:
            pretty_print = "%s(UNINITIALISED)" % parent
        return pretty_print

    @property
    def chromosome(self):
        """ Getter method: retrieve this genotype's chromosome, or throw exception if not yet set """
        if not hasattr(self, "has_value") or not self.has_value:
            raise ValueError("cannot access uninitialised chromosome")

    @chromosome.setter
    @abstractmethod
    def chromosome(self, value):
        """ Placeholder for setting this genotype's chromosome """
        raise NotImplementedError

    @property
    def n_genes(self):
        """ Return the number of genes used by this genotype """
        try:
            return self._n_genes
        except AttributeError:
            raise NotImplementedError

    @abstractmethod
    def factory(self):
        """ Factory method to generate a random individual of this genotype - to be overridden """
        raise NotImplementedError

    def valid_points(self, q):
        """ Helper function to reject potential genotypes that have invalid point configurations """

        # The points must satisfy minimum distance conditions (from borders)
        for q_i in q:
            q_i_x, q_i_y = q_i.allele
            if math.fabs(q_i_x - 0) < self.POINT_DISTANCE_THRESHOLD:
                return False
            if math.fabs(q_i_x - self.width - 1) < self.POINT_DISTANCE_THRESHOLD:
                return False
            if math.fabs(q_i_y - 0) < self.POINT_DISTANCE_THRESHOLD:
                return False
            if math.fabs(q_i_y - self.height - 1) < self.POINT_DISTANCE_THRESHOLD:
                return False

        # The points must satisfy minimum distance conditions (from each other)
        for q_i, q_j in itertools.combinations(q, 2):
            q_i_x, q_i_y = q_i.allele
            q_j_x, q_j_y = q_j.allele
            if math.fabs(q_i_x - q_j_x) < self.POINT_DISTANCE_THRESHOLD:
                return False
            if math.fabs(q_i_y - q_j_y) < self.POINT_DISTANCE_THRESHOLD:
                return False

        return True

    def copy_elite(self):
        """ Safely copy an individual as an elite to the next generation 

            Care must be taken to ensure any internal state is copied into separate memory to avoid further processing
            from inadvertently affecting multiple genotype instances.
        """
        individual = copy.deepcopy(self)  # avoids reuse of memory between related individuals
        individual.phenotype = None  # force an expression when next used
        individual.cached_fitness_value = None  # no expression has occurred, so discard old fitness value
        return individual

    @abstractmethod
    def express(self, name):
        """ Placeholder for genotype's expression function - to be overridden """
        raise NotImplementedError

    def fitness(self, **kwargs):
        """ Generalised fitness function for all genetic algorithm models

            This function implements the core fitness functions for each model:
            1. Traditional genetic algorithm
            2. Epi-genetic algorithm (with exact matches)
            3. Epi-genetic algorithm (with inexact matches)

            While MondrianPhenotype sub-classes may implement different phenotype fitness functions, this detail can be
            abstracted away at the level of computing genotype fitness values for these three models. It is simply
            assumed that MondrianPhenotype sub-classes conform to the interface designed in the abstract base class
            MondrianPhenotype.

            Model #1 simply assigns the phenotype fitness value to the generating genotype. Models #2 and #3 apply
            inference to "share" a phenotype fitness value amoungst all genotypes that could have generated it. The
            actual contribution to each genotype is proportional to the probability that it could have generated the
            phenotype. These model-specific fitness functions are implemented in individual helper functions.
        """

        # If this value has already been computed, use the cached copy to reduce computation
        if self.cached_fitness_value is not None:
            return self.cached_fitness_value

        # Try to extract 'phenotypes' from keyword arguments - all previously evaluated phenotypes
        phenotypes = None
        for key, value in kwargs.items():
            if key == "phenotypes":
                phenotypes = value

        # Compute the genotype fitness value as specified by the GA/EGA model
        if self.model == Model.GA:  # model #1 - traditional genetic algorithm
            # Standard genetic algorithm model
            self.fitness_ga()
        elif self.model == Model.EGA_EXACT_MATCHES:  # model #2 - epi-genetic algorithm (exact matches)
            self.fitness_ega_exact_matches(phenotypes)
        elif self.model == Model.EGA_INEXACT_MATCHES:  # model #3 - epi-genetic algorithm (inexact matches)
            self.fitness_ega_inexact_matches(phenotypes)
        else:
            raise ValueError("unknown evolutionary model")

        self.reachable_phenotypes = None  # free the memory used by the phenotype search
        return self.cached_fitness_value

    def fitness_ga(self):
        """ Genotype fitness function for model #1 - traditional genetic algorithm """
        self.cached_fitness_value = self.phenotype.fitness()
        self.n_epiga_hits = 0  # no other phenotypes were considered

    def fitness_ega_exact_matches(self, phenotypes):
        """ Genotype fitness function for model #2 - epi-genetic algorithm (exact matches) """

        # Pre-compute this outside the loop over possible_phenotypes, it is independent of possible_phenotypes aand slow
        distance = {other: self.phenotype - other for other in phenotypes}

        # Sum over all existing phenotypes
        fitness_numerator = 0
        fitness_denominator = 0
        self.n_epiga_hits = 0
        for possible_phenotype in self.reachable_phenotypes[1]:  # step size of 1 pixel
            for existing_phenotype in phenotypes:
                if possible_phenotype.lines == existing_phenotype.lines:
                    distance_value = distance[existing_phenotype]  # distance between m^{*}(g) and phenotype
                    temp_term = possible_phenotype.p_val * existing_phenotype.fitness()
                    temp_term /= self.weight_k1 * distance_value + 1
                    fitness_numerator += temp_term
                    temp_term = possible_phenotype.p_val * existing_phenotype.MAX_FITNESS
                    temp_term /= self.weight_k1 * distance_value + 1
                    fitness_denominator += temp_term
                    self.n_epiga_hits += 1
        try:
            self.cached_fitness_value = fitness_numerator / fitness_denominator  # epi-genetic model
        except ZeroDivisionError:
            self.cached_fitness_value = 0.0

    def fitness_ega_inexact_matches(self, phenotypes):
        """ Genotype fitness function for model #3 - epi-genetic algorithm (inexact matches) """

        # Pre-compute this outside the loop over possible_phenotypes, it is independent of possible_phenotypes aand slow
        distance = {other: self.phenotype - other for other in phenotypes}

        fitness_numerator = 0
        fitness_denominator = 0
        self.n_epiga_hits = 0
        for existing_phenotype in phenotypes:
            found = False
            for j in sorted(self.reachable_phenotypes.keys()):
                snapped_existing_phenotype = existing_phenotype.snap_to_grid(j)
                for possible_phenotype in self.reachable_phenotypes[j]:
                    if possible_phenotype.lines == snapped_existing_phenotype.lines:
                        distance_value = distance[existing_phenotype]  # distance between m^{*}(g) and phenotype
                        temp_term = possible_phenotype.p_val * existing_phenotype.fitness()
                        temp_term /= (self.weight_k1 * j + 1) * (self.weight_k2 * distance_value + 1)
                        fitness_numerator += temp_term
                        temp_term = possible_phenotype.p_val * existing_phenotype.MAX_FITNESS
                        temp_term /= (self.weight_k1 * j + 1) * (self.weight_k2 * distance_value + 1)
                        fitness_denominator += temp_term
                        self.n_epiga_hits += 1
                        found = True
                        break  # we've found the minimum j s.t. lines are equivalent
                if found:
                    break
        try:
            self.cached_fitness_value = fitness_numerator / fitness_denominator  # epi-genetic model
        except ZeroDivisionError:
            self.cached_fitness_value = 0.0
