#include <cassert>
#include <cmath>
#include <iostream>
#include <set>
#include <vector>
#include "phenotype.hpp"

/*
 * Phenotype constructor.
 */
Phenotype::Phenotype(int width, int height, int n_loops, PointList points, std::vector<double> direction_probability)
{
    /* Store all parameters as member attributes */
    this->_width = width;
    this->_height = height;
    this->_n_loops = n_loops;
    this->_n_points = (int)points.size();
    this->_points = points;
    this->_direction_probability = direction_probability;

    /* Initialise a vector of available directions for each generating point */
    for (Direction direction = NORTH; direction < LAST; direction++)
    {
        this->_available[direction].resize((unsigned int)(this->_n_points));
        for (int i = 0; i < this->_n_points; i++)
        {
            this->_available[direction][i] = true; /* default: point unencumbered in direction */
            if ((direction == NORTH) && (points[i].second == 0))
            {
                this->remove_direction(i, direction); /* can't go past the northern border */
            }
            else if ((direction == SOUTH) && (points[i].second == (height - 1)))
            {
                this->remove_direction(i, direction); /* can't go past the southern border */
            }
            else if ((direction == EAST) && (points[i].first == (width - 1)))
            {
                this->remove_direction(i, direction); /* can't go past the eastern border */
            }
            else if ((direction == WEST) && (points[i].first == 0))
            {
                this->remove_direction(i, direction); /* can't go past the western border */
            }
        }
    }

    this->_p_val = 1.0;
    this->_step_size = 1;
}

/*
 * Copy constructor: produce a new Phenotype object the same state as instance, with deep-copies of member attributes.
 */
Phenotype::Phenotype(const Phenotype & instance)
{
    this->_width = instance._width;
    this->_height = instance._height;
    this->_n_loops = instance._n_loops;
    this->_n_points = instance._n_points;
    this->_points = instance._points;
    this->_direction_probability = instance._direction_probability; /* std::vector handles deep-copy */
    this->_horizontal = instance._horizontal; /* std::set handles deep-copy */
    this->_vertical = instance._vertical; /* std::set handles deep-copy */

    for (Direction direction = NORTH; direction < LAST; direction++)
    {
        this->_available[direction] = instance._available[direction]; /* std::vector handles deep-copy */
    }

    this->_p_val = instance._p_val;
    this->_step_size = instance._step_size;
}

/*
 * Destructor
 */
Phenotype::~Phenotype()
{
    /* Explicitly free heap-memory for STL data-structures */
    this->_points.clear();
    this->_direction_probability.clear();
    this->_horizontal.clear();
    this->_vertical.clear();
    for (Direction direction = NORTH; direction < LAST; direction++)
    {
        this->_available[direction].clear();
    }
}

/*
 * Less than comparison operator overload; logic is delegated to std::set less than operator.
 */
bool Phenotype::operator<(const Phenotype & other) const
{
    /*
     * Arbitrarily and without loss of generality horizontal lines are given precedence over vertical lines. This is
     * simply for comparison's sake and to define a Phenotype ordering. This ordering isn't used anywhere; however this
     * comparison is needed by unordered_map to identify when one Phenotype is equivalent to another.
     */
    return (this->horizontal() < other.horizontal()) ||
           ((this->horizontal() == other.horizontal()) && (this->vertical() < other.vertical()));
}

/*
 * Equality comparison operator overload; logic is delegated to std::set equality operator.
 */
bool Phenotype::operator==(const Phenotype & other) const
{
    return (this->horizontal() == other.horizontal()) && (this->vertical() == other.vertical());
}

/*
 * Getter method to provide read-only access to horizontal lines member attribute. Applies grid granularity (step_size).
 */
std::set<Line> Phenotype::horizontal() const
{
    std::set<Line> result;
    std::set<Line>::iterator it;

    for (it = this->_horizontal.begin(); it != this->_horizontal.end(); it++)
    {
        Point P = it->first;
        Point Q = it->second;
        int Px = std::min(this->snap_to_grid(P.first), this->_width - 1); /* ensure line doesn't go past border */
        int Py = std::min(this->snap_to_grid(P.second), this->_height - 1); /* ensure line doesn't go past border */
        int Qx = std::min(this->snap_to_grid(Q.first), this->_width - 1); /* ensure line doesn't go past border */
        int Qy = std::min(this->snap_to_grid(Q.second), this->_height - 1); /* ensure line doesn't go past border */
        Point P_snapped = Point(Px, Py);
        Point Q_snapped = Point(Qx, Qy);
        if ((P_snapped != Q_snapped) && (Py > 0) && (Py < (this->_height - 1)))
        {
            /* Only add the line if it has non-zero length and it doesn't run along a border */
            result.insert(Line(P_snapped, Q_snapped));
        }
    }

    return this->merge_lines_horizontal(result);
}

/*
 * Getter method to provide read-only access to vertical lines member attribute. Applies grid granularity (step_size).
 */
std::set<Line> Phenotype::vertical() const
{
    std::set<Line> result;
    std::set<Line>::iterator it;

    for (it = this->_vertical.begin(); it != this->_vertical.end(); it++)
    {
        Point P = it->first;
        Point Q = it->second;
        int Px = std::min(this->snap_to_grid(P.first), this->_width - 1); /* ensure line doesn't go past border */
        int Py = std::min(this->snap_to_grid(P.second), this->_height - 1); /* ensure line doesn't go past border */
        int Qx = std::min(this->snap_to_grid(Q.first), this->_width - 1); /* ensure line doesn't go past border */
        int Qy = std::min(this->snap_to_grid(Q.second), this->_height - 1); /* ensure line doesn't go past border */
        Point P_snapped = Point(Px, Py);
        Point Q_snapped = Point(Qx, Qy);
        if ((P_snapped != Q_snapped) && (Px > 0) && (Px < (this->_width - 1)))
        {
            /* Only add the line if it has non-zero length and it doesn't run along a border */
            result.insert(Line(P_snapped, Q_snapped));
        }
    }

    return this->merge_lines_vertical(result);
}

/*
 * Getter method to provide access to Phenotype probability member attribute.
 */
double Phenotype::p_val()
{
    return this->_p_val;
}

/*
 * Setter method to set Phenotype probability member attribute.
 */
void Phenotype::set_p_val(double p_val)
{
    this->_p_val = p_val;
}

/*
 * Method to accumulate (multiplicatively) the Phenotype probability member attribute.
 */
void Phenotype::accumulate_p_val_mul(double p_val)
{
    this->_p_val *= p_val;
}

/*
 * Method to accumulate (additively) the Phenotype probability member attribute.
 */
void Phenotype::accumulate_p_val_add(double p_val)
{
    this->_p_val += p_val;
}

/*
 * Main genotype to phenotype enumeration helper function.
 *
 * This function is called when a decision has been made to draw *a* line from the generating point indicated by
 * point_index; further, the probability of this decision being made is given by threshold.
 *
 * This function must therefore perform all further branching:
 * - Enumerate all valid directions
 * - For each valid direction, enumerate all valid end-points for the emitted line to terminate
 *
 * Every combination of these choices will result in a new Phenotype. Each new Phenotype shares the same history up
 * until the point that this function was called. Their probabilities and horizontal/vertical lines will now probably
 * diverge.
 */
std::vector<Phenotype> Phenotype::derive_phenotypes(int point_index, double threshold)
{
    std::vector<Phenotype> result;

    for (Direction direction = NORTH; direction < LAST; direction++)
    {
        if (this->direction_probability(point_index, direction) > 0)
        {
            /* This is a valid direction, branch */
            std::set<Point> line_ends = this->find_line_ends(this->_points[point_index], direction);
            for (std::set<Point>::iterator it = line_ends.begin(); it != line_ends.end(); it++)
            {
                /* This is a valid line end-point, branch */
                Phenotype new_phenotype = Phenotype(*this);
                new_phenotype.add_line(point_index, *it, direction, threshold / line_ends.size());
                result.push_back(new_phenotype);
            }
        }
    }

    return result;
}

/*
 * Secondary genotype to phenotype enumeration helper function.
 *
 * This function is called to enumerate all possible ways of making valid Phenotypes from the conclusion of the general
 * enumeration procedure. This is needed because the resultant Phenotypes at that point may violate Mondrian rules.
 *
 * This function must therefore perform all further branching:
 * - Enumerate over points until they are valid (i.e. don't have a floating line or right angle connected)
 * - While points are invalid, enumerate remaining valid line directions (and end-points) until all Phenotypes are valid
 *
 * As per Phenotype::derive_phenotypes, each branch/choice will result in a new Phenotype.
 */
std::vector<Phenotype> Phenotype::derive_valid_phenotypes()
{
    std::vector<Phenotype> result;
    std::vector<Phenotype>::iterator it;
    bool this_is_valid = true;

    for (int i = 0; i < this->_n_points; i++)
    {
        if (this->invalid_point(i))
        {
            /* This point violates Mondrian rules, fix it (in all possible ways) */
            std::vector<Phenotype> new_phenotypes = this->derive_phenotypes(i, 1.0); /* threshold == 1.0, take action */
            for (it = new_phenotypes.begin(); it != new_phenotypes.end(); it++)
            {
                std::vector<Phenotype> next = it->derive_valid_phenotypes(); /* recursively make these valid */
                result.insert(result.end(), next.begin(), next.end());
            }
            this_is_valid = false;
            break;
        }
    }

    if (this_is_valid)
    {
        /* Recursive base case, return single Phenotype unchanged */
        result.push_back(*this);
    }

    return result;
}

/*
 * Helper function to provide probability of emitting a line from the given generating point. This probability is a
 * function of the existing lines already emitted and was derived from real-world Mondrian artwork in an attempt to
 * approximate his works.
 *
 * The probabilities contained in this function come directly from the earlier works of Jian Yin Shen:
 *     Title: Cyber-Genetic Neo-Plasticism - An AI program Creating Mondrian-like Paintings by using Interactive
 *     Bacterial Evolution Algorithm
 *     Authors: Jian Yin Shen, Tom Gedeon
 */
double Phenotype::emission_probability(int point_index)
{
    int n_remaining_directions = 0;

    /* Count the number of remaining valid directions */
    for (Direction direction = NORTH; direction < LAST; direction++)
    {
        if (this->direction_probability(point_index, direction) > 0)
        {
            n_remaining_directions++;
        }
    }

    /* Certain point configurations are given simply probabilities */
    switch (n_remaining_directions)
    {
        case 4:
            return 1.0; /* point has no lines emitted, always draw */
        case 3:
            return 0.9; /* point has a single line emitted, high likelihood of drawing */
        case 1:
            return 0.2; /* point forms a T-intersection, low likelihood of drawing the "boring" cross point */
        case 0:
            return 0.0; /* point is already a cross point, no further lines *can* be drawn */
    }

    /* Handle the cases where two lines have been emitted from the generating point */
    if ((this->direction_probability(point_index, NORTH) > 0) &&
        (this->direction_probability(point_index, SOUTH) > 0))
    {
        return 0.3; /* connected horizontal line, low likelihood of drawing */
    }
    else if ((this->direction_probability(point_index, EAST) > 0) &&
             (this->direction_probability(point_index, WEST) > 0))
    {
        return 0.3; /* connected vertical line, low likelihood of drawing */
    }
    else
    {
        return 1.0; /* right-angle, this is illegal, always draw */
    }
}

/*
 * Helper function to provide probability of emitting a line from the given generating point in the given direction.
 * This probability is a function of the genotype's probability distribution and existing lines already emitted that are
 * used to normalise the genotype's probability distribution.
 */
double Phenotype::direction_probability(int point_index, Direction direction)
{
    double sub_total = 0.0;
    for (Direction d = NORTH; d < LAST; d++)
    {
        if (this->_available[d][point_index])
        {
            sub_total += this->_direction_probability[d];
        }
    }
    if ((sub_total == 0.0) || (!this->_available[direction][point_index]))
    {
        return 0.0; /* this direction is not available, so its probability is 0 */
    }
    else
    {
        return this->_direction_probability[direction] / sub_total; /* normalise to ignore already drawn directions */
    }
}

/*
 * Set the grid granularity of this Phenotype
 */
void Phenotype::set_step_size(int step_size)
{
    this->_step_size = step_size;
}

/*
 * Merge any overlapping/connected/redundant horizontal and vertical lines
 */
void Phenotype::merge_lines()
{
    this->_horizontal = this->merge_lines_horizontal(this->_horizontal);
    this->_vertical = this->merge_lines_vertical(this->_vertical);
}

/*
 * Pretty-print function to output the description of a Phenotype to stdout
 *
 * The output consists of:
 * 1. The probability that this Phenotype is generated by the genotype
 * 2. A list of horizontal line segments
 * 3. A list of vertical line segments
 *
 * Output format is designed to allow easy parsing of a consuming application where many Phenotypes may describe
 * themselves in succession.
 */
void Phenotype::describe()
{
    std::set<Line>::iterator it;
    std::set<Line> horizontal = this->horizontal();
    std::set<Line> vertical = this->vertical();

    std::cout << "P-VALUE " << this->_p_val << '\n';

    std::cout << "STEP SIZE " << this->_step_size << '\n';

    std::cout << "HORIZONTAL LINES\n";
    for (it = horizontal.begin(); it != horizontal.end(); it++)
    {
        Point P = it->first;
        Point Q = it->second;
        std::cout << P.first << ',' << P.second << "->" << Q.first << ',' << Q.second << '\n';
    }

    std::cout << "VERTICAL LINES\n";
    for (it = vertical.begin(); it != vertical.end(); it++)
    {
        Point P = it->first;
        Point Q = it->second;
        std::cout << P.first << ',' << P.second << "->" << Q.first << ',' << Q.second << '\n';
    }
}

/*
 * Helper function to mark Direction 'direction' from the point indicated by 'point_index' as invalid.
 */
void Phenotype::remove_direction(int point_index, Direction direction)
{
    this->_available[direction][point_index] = false;
}

/*
 * A line will be emitted from the point 'origin' in Direction 'direction'. This function calculates all possible
 * end-points for said line; namely:
 * - The border of the canvas
 * - Any existing line intersected along the way
 *
 * This function returns a set of all possible end-points.
 */
std::set<Point> Phenotype::find_line_ends(Point origin, Direction direction)
{
    std::set<Point> result;
    std::set<Line>::iterator it;

    /*
     * This will appear odd. This function should *not* include origin in its results, which would be used to produce
     * a line of zero length. However, there are many edge-cases that can result in the following logic producing
     * 'origin' as a valid end-point. The simplest solution is to add it to the std::set result set and then remove it
     * immediately before returning from this function.
     *
     * This way it can safely be removed (because it's added here) but any case where it is added is ignored. A std::set
     * cannot contain duplicate values.
     */
    result.insert(origin);

    /*
     * Given direction, we draw a line from origin to the canvas border. We then attempt to intersect all orthogonal
     * lines. To do so, denote four points {N,S,E,W} as the upper/lower points of the vertical line and right/left
     * points of the horizontal line respectively. Depending on 'direction', one of these lines will be fixed and the
     * other will be given by an enumeration over a set of existing horizontal/vertical lines. Either way, this naming
     * convention remains constant.
     *
     * With this convention, simply tests can show whether the lines N->S and W->E actually intersect.
     */
    if ((direction == NORTH) || (direction == SOUTH))
    {
        Point N, S;
        if (direction == NORTH)
        {
            N = Point(origin.first, 0); /* northern canvas border */
            S = origin;
            result.insert(N);
        }
        else if (direction == SOUTH)
        {
            N = origin;
            S = Point(origin.first, this->_height - 1); /* southern canvas border */
            result.insert(S);
        }
        for (it = this->_horizontal.begin(); it != this->_horizontal.end(); it++)
        {
            Point W = it->first;
            Point E = it->second;
            if ((W.first <= N.first) && (N.first <= E.first) &&
                (N.second <= W.second) && (W.second <= S.second))
            {
                result.insert(Point(N.first, W.second));
            }
        }
    }
    else if ((direction == EAST) || (direction == WEST))
    {
        Point E, W;
        if (direction == EAST)
        {
            E = Point(this->_width - 1, origin.second); /* eastern canvas border */
            W = origin;
            result.insert(E);
        }
        else if (direction == WEST)
        {
            E = origin;
            W = Point(0, origin.second); /* western canvas border */
            result.insert(W);
        }
        for (it = this->_vertical.begin(); it != this->_vertical.end(); it++)
        {
            Point N = it->first;
            Point S = it->second;
            if ((W.first <= N.first) && (N.first <= E.first) &&
                (N.second <= W.second) && (W.second <= S.second))
            {
                result.insert(Point(N.first, W.second));
            }
        }
    }
    else
    {
        std::cerr << "Error: bad direction.\n";
    }

    /* As per the comments above, remove origin as a possible end-point to avoid zero-length lines */
    result.erase(origin);

    return result;
}

/*
 * Draw a line in Direction 'direction' from the point indicated by point_index to Q. The probability that this action
 * was taken is given by 'threshold'. This is not used to make decisions in this function but is used to update the
 * Phenotype's probability value.
 *
 * The line will be in a canonical form; namely:
 * - horizontal lines are described in the West to East direction
 * - vertical lines are described in the North to South direction
 *
 * These forms correspond to the numeric ordering of pixels in most drawing/graphing libraries.
 */
void Phenotype::add_line(int point_index, Point Q, Direction direction, double threshold)
{
    assert((0 <= point_index) && ((unsigned int)point_index < this->_points.size()));
    Point P = this->_points[point_index];

    assert(this->_available[direction][point_index]);
    assert(P != Q);
    assert((0 <= P.first) && (P.first < this->_width));
    assert((0 <= P.second) && (P.second < this->_height));
    assert((0 <= Q.first) && (Q.first < this->_height));
    assert((0 <= Q.second) && (Q.second < this->_height));

    /* Canonicalise the line */
    switch (direction)
    {
        case NORTH:
            assert(P.first == Q.first);
            this->_vertical.insert(Line(Q, P));
            break;
        case SOUTH:
            assert(P.first == Q.first);
            this->_vertical.insert(Line(P, Q));
            break;
        case EAST:
            assert(P.second == Q.second);
            this->_horizontal.insert(Line(P, Q));
            break;
        case WEST:
            assert(P.second == Q.second);
            this->_horizontal.insert(Line(Q, P));
            break;
        default:
            std::cerr << "Error: bad direction.\n";
    }

    /* Update probability value and available directions */
    this->accumulate_p_val_mul(this->direction_probability(point_index, direction) * threshold);
    this->remove_direction(point_index, direction);
}

/*
 * Helper function to recursively merge horizontal lines. Lines may be merged when they:
 * - overlap
 * - connect to form a single line
 */
std::set<Line> Phenotype::merge_lines_horizontal(std::set<Line> lines) const
{
    std::set<Line>::iterator it1;
    std::set<Line>::iterator it2;

    for (it1 = lines.begin(); it1 != lines.end(); it1++)
    {
        for (it2 = lines.begin(); it2 != lines.end(); it2++)
        {
            if (*it1 != *it2)
            {
                Point W1 = it1->first;
                Point E1 = it1->second;
                Point W2 = it2->first;
                Point E2 = it2->second;

                if ((W1.second == W2.second) && (W1.first <= W2.first) && (E2.first <= E1.first))
                {
                    /* W2->E2 is entirely contained within W1->E1 */
                    lines.erase(Line(W2, E2));
                    return this->merge_lines_horizontal(lines);
                }
                else if ((W1.second == W2.second) && (W2.first <= W1.first) && (E1.first <= E2.first))
                {
                    /* W1->E1 is entirely contained within W2->E2 */
                    lines.erase(Line(W1, E1));
                    return this->merge_lines_horizontal(lines);
                }
                else if ((W1.second == W2.second) && (E1.first == W2.first))
                {
                    /* (W1->E1) -> (W2->E2) forms a solid line */
                    lines.erase(Line(W1, E1));
                    lines.erase(Line(W2, E2));
                    lines.insert(Line(W1, E2));
                    return this->merge_lines_horizontal(lines);
                }
                else if ((W1.second == W2.second) && (E2.first == W1.first))
                {
                    /* (W2->E2) -> (W1->E1) forms a solid line */
                    lines.erase(Line(W1, E1));
                    lines.erase(Line(W2, E2));
                    lines.insert(Line(W2, E1));
                    return this->merge_lines_horizontal(lines);
                }
                else if ((W1.second == W2.second) && (W1.first <= W2.first) && (W2.first <= E1.first))
                {
                    /* (W1->E1) overlaps (W2->E2) from the left */
                    lines.erase(Line(W1, E1));
                    lines.erase(Line(W2, E2));
                    lines.insert(Line(W1, E2));
                    return this->merge_lines_horizontal(lines);
                }
                else if ((W1.second == W2.second) && (W2.first <= W1.first) && (W1.first <= E2.first))
                {
                    /* (W2->E2) overlaps (W1->E1) from the left */
                    lines.erase(Line(W1, E1));
                    lines.erase(Line(W2, E2));
                    lines.insert(Line(W2, E1));
                    return this->merge_lines_horizontal(lines);
                }
            }
        }
    }

    return lines;
}

/*
 * Helper function to recursively merge vertical lines. Lines may be merged when they:
 * - overlap
 * - connect to form a single line
 */
std::set<Line> Phenotype::merge_lines_vertical(std::set<Line> lines) const
{
    std::set<Line>::iterator it1;
    std::set<Line>::iterator it2;

    for (it1 = lines.begin(); it1 != lines.end(); it1++)
    {
        for (it2 = lines.begin(); it2 != lines.end(); it2++)
        {
            if (*it1 != *it2)
            {
                Point N1 = it1->first;
                Point S1 = it1->second;
                Point N2 = it2->first;
                Point S2 = it2->second;

                if ((N1.first == N2.first) && (N1.second <= N2.second) && (S2.second <= S1.second))
                {
                    /* N2->S2 is entirely contained within N1->S1 */
                    lines.erase(Line(N2, S2));
                    return this->merge_lines_vertical(lines);
                }
                else if ((N1.first == N2.first) && (N2.second <= N1.second) && (S1.second <= S2.second))
                {
                    /* N1->S1 is entirely contained within N2->S2 */
                    lines.erase(Line(N1, S1));
                    return this->merge_lines_vertical(lines);
                }
                else if ((N1.first == N2.first) && (S1.second == N2.second))
                {
                    /* (N1->S1) -> (N2->S2) forms a solid line */
                    lines.erase(Line(N1, S1));
                    lines.erase(Line(N2, S2));
                    lines.insert(Line(N1, S2));
                    return this->merge_lines_vertical(lines);
                }
                else if ((N1.first == N2.first) && (S2.second == N1.second))
                {
                    /* (N2->S2) -> (N1->S1) forms a solid line */
                    lines.erase(Line(N1, S1));
                    lines.erase(Line(N2, S2));
                    lines.insert(Line(N2, S1));
                    return this->merge_lines_vertical(lines);
                }
                else if ((N1.first == N2.first) && (N1.second <= N2.second) && (N2.second <= S1.second))
                {
                    /* (N1->S1) overlaps (N2->S2) from above */
                    lines.erase(Line(N1, S1));
                    lines.erase(Line(N2, S2));
                    lines.insert(Line(N1, S2));
                    return this->merge_lines_horizontal(lines);
                }
                else if ((N1.first == N2.first) && (N2.second <= N1.second) && (N1.second <= S2.second))
                {
                    /* (N2->S2) overlaps (N1->S1) from above */
                    lines.erase(Line(N1, S1));
                    lines.erase(Line(N2, S2));
                    lines.insert(Line(N2, S1));
                    return this->merge_lines_horizontal(lines);
                }
            }
        }
    }

    return lines;
}

/*
 * Boolean function indicating whether the point indicated by 'point_index' is invalid. A point can be invalid by
 * violating one of the Mondrian artwork rules:
 * 1. A point must not form a right-angle
 * 2. A line cannot be hanging on the canvas (i.e. a point cannot have only one line emitted from it)
 */
bool Phenotype::invalid_point(int point_index)
{
    int n_horizontal_options = 0;
    int n_vertical_options = 0;
    int n_options = 0;

    /* Count the available options in total, and grouped by horizontal and vertical directions */
    for (Direction direction = NORTH; direction < LAST; direction++)
    {
        if (this->_available[direction][point_index])
        {
            if ((direction == NORTH) || (direction == SOUTH))
            {
                n_vertical_options++;
            }
            else if ((direction == EAST) || (direction == WEST))
            {
                n_horizontal_options++;
            }
            n_options++;
        }
    }

    /* Test both cases of illegal points */
    bool right_angle = (n_options == 2) && (n_horizontal_options == 1) && (n_vertical_options == 1);
    bool online = (n_options == 3);

    return right_angle || online;
}

/*
 * Round value to its nearest multiple of the current step size. Used to force Line instances to lie on specific
 * grid granularities.
 */
int Phenotype::snap_to_grid(int value) const
{
    return int(round(float(value) / this->_step_size) * this->_step_size);
}

/*
 * Post-fix increment of a Direction to allow it to be used in a for-loop
 */
Direction operator++(Direction & direction, int)
{
    Direction existing = direction;
    int next_direction = ((int)direction) + 1;
    direction = (Direction)(next_direction);
    return existing;
}
