from abc import ABCMeta
from copy import deepcopy
from gene import Gene
from PIL import Image, ImageDraw
from subprocess import Popen, PIPE
from template import GADescription
from util import fitness_proportionate_selection, luck, discretise_integer, parse_phenotypes
import itertools
import math
import os
import random


class MondrianPhenotype(GADescription):
    """ Mondrian-like phenotype functionality common across all implementations """

    __metaclass__ = ABCMeta

    # Colour RGB codes sourced from Jian's original Darwindrian work
    WHITE = (230, 230, 224)
    BLUE = (13, 79, 158)
    YELLOW = (245, 212, 71)
    RED = (169, 33, 42)
    BLACK = (15, 15, 15)

    STROKE_WIDTH = 9  # width of a line segment (in pixels)

    # Experimental constants
    DIRECTIONS = ["North", "South", "East", "West"]
    COLOURS = [RED, BLUE, YELLOW]
    EMISSION_PROB = {"Initial": 1.0, "Terminal": 0.9, "RightAngle": 1.0, "OnLine": 0.3, "Nodal": 0.2, "Cross": 0.0}

    ORIENTATIONS = ["horizontal", "vertical"]  # possible line orientations (used to categorise line segments)

    # Placeholder lists of structural/colour ratings that carry positive ratings - to be populated by sub-classes
    POSITIVE_STRUCTURE_RATING = []
    POSITIVE_COLOUR_RATING = []

    def __init__(self, name, **kwargs):
        """ Keyword argument aware constructor """

        self.name = name  # name will be used to create final image filename

        # Process all keyword arguments - make them into instance attributes
        for key, value in kwargs.items():
            if isinstance(value, Gene):
                setattr(self, key, value.allele)  # pull the allele from the Gene object
            else:
                setattr(self, key, value)

        # Initialise default values for various attributes
        self._rating = None
        self.lines = {"horizontal": set(), "vertical": set()}
        self.points = set()
        self.filled_rectangles = set()
        self.w_min = None
        self.w_max = None
        self.h_min = None
        self.h_max = None
        self.p_val = None

        # Define lines on the canvas borders
        self.borders = \
        {
            "horizontal":
                [((0, 0), (self.width - 1, 0)), ((0, self.height - 1), (self.width - 1, self.height - 1))],
            "vertical":
                [((0, 0), (0, self.height - 1)), ((self.width - 1, 0), (self.width - 1, self.height - 1))]
        }

    def __deepcopy__(self, memodict={}):
        """ Function invoked on copy.deepcopy(self)

            Object deepcopy turned out to be a major bottleneck, thus this custom deepcopy constructor was developed to
            reduce runtime. This is achieved by explicitly copying all member attributes in type-specific ways.

            Most member attributes are immutable and can safely be copied via the dict.update function. There are a few
            mutable attributes however that need special treatment:
            - self.lines = {str: set}
            - self.points = set
            - self.filled_rectangles = set
            New sets are constructed, the elements of these respective sets are copied using set comprehension.
        """
        result = type(self)(name=self.name, width=self.width, height=self.height)  # create new object of self's type
        result.__dict__.update(self.__dict__)  # copy all member attributes (i.e. __dict__)
        result.lines = {"horizontal": {line for line in self.lines["horizontal"]},
                        "vertical": {line for line in self.lines["vertical"]}}
        result.points = {point for point in self.points}
        result.filled_rectangles = {filled_rectangle for filled_rectangle in self.filled_rectangles}
        return result

    def __sub__(self, other):
        """ Custom MondrianPhenotype difference operator

            The difference of two MondrianPhenotype instances is defined as the number of dis-similar pixel RGB values.
            Should one MondrianPhenotype have a different number of pixels to the other, the smaller image is extended
            with placeholder values that will evaluate as dis-similar to any pixel RGB value.

            Caution: the difference between a MondrianPhenotype and any other class instance is not defined.
        """
        assert isinstance(other, MondrianPhenotype), "can only compute the difference between MondrianPhenotype objects"
        self_pixel_data = self.render(save_image=False)  # False => do not write image file to disk
        other_pixel_data = other.render(save_image=False)  # False => do not write image file to disk
        pixel_iter = itertools.zip_longest(self_pixel_data, other_pixel_data)
        dissimilar_pixels = (1 if a != b else 0 for a, b in pixel_iter)
        return sum(dissimilar_pixels)  # total number of dis-similar pixels

    def key_fields(self):
        """ Construct an immutable, iterable, collection of the key attributes that define a MondrianPhenotype

            While sub-classes of MondrianPhenotype will differ in some implementation details, they are ultimately
            defined by their geometries (line segments and coloured rectangles).
        """
        return frozenset(self.lines["horizontal"]), frozenset(self.lines["vertical"]), frozenset(self.filled_rectangles)

    @property
    def lines_hash(self):
        """ Map self.lines to an immutable object so that it can be hashed/used as a dictionary key """
        return frozenset((frozenset(self.lines["horizontal"]), frozenset(self.lines["vertical"])))

    def get_rating(self):
        """ Getter method for MondrianPhenotype rating """
        return self._rating

    def set_rating(self, value):
        """ Setter method for MondrianPhenotype rating """
        self._rating = value

    @property
    def structure_rating(self):
        """ (Read-only) getter method for numeric MondrianPhenotype structural rating (pure function of user rating) """
        return self.rating in self.POSITIVE_STRUCTURE_RATING

    @property
    def colour_rating(self):
        """ (Read-only) getter method for numeric MondrianPhenotype colour rating (pure function of user rating) """
        return self.rating in self.POSITIVE_COLOUR_RATING

    def find_line_ends(self, q, d):
        """ Identify possible end-points for a line drawn from point 'q' in direction 'd'

            Valid end-points are:
            - the border of the canvas
            - any existing line that could be intersected from this operation

            All valid end-points are given uniform probability of selection
        """

        # Find the point on the canvas border
        x, y = q
        other = {"North": (x, 0), "South": (x, self.height - 1), "East": (self.width - 1, y), "West": (0, y)}
        border_point = other[d]

        # Find any other points of intersection (with other line segments)
        candidates = []
        if d in ["North", "South"]:
            for h_line in self.lines["horizontal"].union(self.borders["horizontal"]):
                if MondrianPhenotype.intersect(h_line, (q, border_point)):
                    candidates.append((q[0], h_line[0][1]))
        elif d in ["East", "West"]:
            for v_line in self.lines["vertical"].union(self.borders["vertical"]):
                if MondrianPhenotype.intersect((q, border_point), v_line):
                    candidates.append((v_line[0][0], q[1]))
        else:
            raise ValueError

        # Return all candidate end-points
        candidates = [pt for pt in candidates if pt != q]  # pt != q ensures we don't have a 0-length line (i.e. point)
        if len(candidates) == 0:
            candidates = [border_point]  # the only valid end-point is on the canvas border, select it
        return candidates

    def add_line(self, origin, end_point):
        """ A line will be drawn between the points 'origin' and 'end_point' """
        if origin[0] == end_point[0]:
            orientation = "vertical"
        elif origin[1] == end_point[1]:
            orientation = "horizontal"
        else:
            raise ValueError("line must be horizontal or vertical")
        if origin == end_point:
            raise ValueError("line must have non-zero length")
        self.lines[orientation].add(MondrianPhenotype.canonical_line(origin, end_point))

    def add_random_line(self, origin, direction):
        """ Draw a line from 'origin' in 'direction', terminating at a valid end-point """
        line_ends = self.find_line_ends(origin, direction)
        line_end = random.choice(line_ends)  # select a point of intersection uniformly at random
        orientation = {"North": "vertical", "South": "vertical", "East": "horizontal", "West": "horizontal"}[direction]
        self.add_line(origin, line_end)

    def add_point(self, p):
        """ Draw a point at location 'p' """
        self.points.add(p)

    def snap_to_grid(self, granularity):
        """ Lock all lines of the current phenotype to a grid of a given granularity """

        # A grid with granularity one is equivalent to the current (integer based) grid
        if granularity == 1:
            return self

        # The granularity is not one, the phenotype will change - so, make a copy and work on that
        result = deepcopy(self)

        # Perform grid alignment
        for orientation in self.ORIENTATIONS:
            new_lines = set()
            for line in self.lines[orientation]:
                pt1, pt2 = line
                pt1 = (discretise_integer(pt1[0], granularity), discretise_integer(pt1[1], granularity))
                pt2 = (discretise_integer(pt2[0], granularity), discretise_integer(pt2[1], granularity))
                if (pt1 != pt2) and (orientation == "horizontal") and (0 < pt1[1] < (self.height - 1)):
                    pt1, pt2 = MondrianPhenotype.canonical_line(pt1, pt2)
                    pt1 = (max(pt1[0], 0), pt1[1])
                    pt2 = (min(pt2[0], self.width - 1), pt2[1])
                    new_lines.add((pt1, pt2))
                elif (pt1 != pt2) and (orientation == "vertical") and (0 < pt1[0] < (self.width - 1)):
                    pt1, pt2 = MondrianPhenotype.canonical_line(pt1, pt2)
                    pt1 = (pt1[0], max(pt1[1], 0))
                    pt2 = (pt2[0], min(pt2[1], self.height - 1))
                    new_lines.add((pt1, pt2))
            result.lines[orientation] = new_lines
        result.__merge_lines()  # merge any lines colliding under the new granularity

        return result

    def finalise(self):
        """ Finalise the Mondrian-like image - i.e. select rectangles to colour """
        self.colour_rectangles()

    def render(self, save_image=True):
        """ Render the Mondrian-like image using Pillow, optionally produce an image file

            Render may be called with save_image=False to allow for image differences to be computed without actually
            writing the phenotype to disk. This is needed during the epi-genetic search logic.

            If save_image=True (default case) then all in memory data is destroyed to avoid memory explosions.
        """

        # Initialise drawing objects from the Python pillow library
        img = Image.new("RGB", (self.width, self.height), MondrianPhenotype.WHITE)
        draw = ImageDraw.Draw(img)

        # First, draw the rectangles
        for p1, p2, fill_colour in self.filled_rectangles:
            draw.rectangle([p1, p2], fill=fill_colour)

        # Second, draw lines (over the top of rectangles where relevant)
        for orientation in self.lines.keys():
            for line in self.lines[orientation]:
                draw.line(line, fill=MondrianPhenotype.BLACK, width=MondrianPhenotype.STROKE_WIDTH)

        # Optionally, show the origin points if requested (not valid Mondrian, but useful for debugging)
        if self.show_points:
            radius = int(MondrianPhenotype.STROKE_WIDTH / 2)
            for x, y in self.points:
                draw.ellipse((x - radius, y - radius, x + radius, y + radius), fill=MondrianPhenotype.WHITE)

        if save_image:
            # Finally, produce the image final and delete non-serialisable objects to allow web UI to maintain state
            img.save("%s.png" % os.path.join(self.exp_path, self.name))
        else:
            # Alternatively, extract the pixel data for all colour lanes - used for image differences
            return img.getdata()

    def colour_rectangles(self):
        """ Choose and colour rectangles based on probabilities and thresholds from key genotype genes """

        # Merge any line segments drawn in opposite directions to one-another
        self.__merge_lines()

        # Find all rectangles that can be coloured (based on key genotype genes)
        rectangles = self.find_rectangles()

        # Select and colour some of the possible rectangles
        n_rectangles = 0
        while len(rectangles) > 0 and n_rectangles < self.c_max:
            r = random.choice(rectangles)
            c = fitness_proportionate_selection(self.c)
            p1, p2 = r
            self.add_rectangle(p1, p2, c)
            n_rectangles += 1
            rectangles.remove(r)  # don't allow re-selection of exactly the same rectangle

    def find_rectangles(self):
        """ Find all rectangles between lines and borders in the phenotype """

        # Must consider borders as possible rectangle edges
        borders = {
            "horizontal": [((0, 0), (self.width - 1, 0)), ((0, self.height - 1), (self.width - 1, self.height - 1))],
            "vertical": [((0, 0), (0, self.height - 1)), ((self.width - 1, 0), (self.width - 1, self.height - 1))]
        }

        # Take pairs of horizontal lines and find any possible vertical line pairs that form a rectangle
        rv = []
        for h1, h2 in itertools.combinations(self.lines["horizontal"].union(borders["horizontal"]), 2):
            possible = []
            for v in self.lines["vertical"].union(borders["vertical"]):
                if MondrianPhenotype.intersect(h1, v) and MondrianPhenotype.intersect(h2, v):
                    possible.append(v)
            for v1, v2 in itertools.combinations(possible, 2):
                if self.eligible_rectangle(h1, h2, v1, v2):
                    rv.append(((v1[0][0], h1[0][1]), (v2[0][0], h2[0][1])))
        return rv

    def add_rectangle(self, p1, p2, fill_colour):
        """ Record the rectangle with corners at 'p1,p2' and a colour fill 'color'

            Not actually rendered at this point, but the location and colour is set here.
        """

        # Draw/fill the rectangle with randomly selected colour
        self.filled_rectangles.add((p1, p2, fill_colour))

        # Accumulate min and max values (so far) of the width and height of drawn rectangles in this phenotype
        w, h = int(math.fabs(p1[0] - p2[0])), int(math.fabs(p1[1] - p2[1]))
        self.w_min = w if self.w_min is None else min(w, self.w_min)
        self.w_max = w if self.w_max is None else max(w, self.w_max)
        self.h_min = w if self.h_min is None else min(h, self.h_min)
        self.h_max = w if self.h_max is None else max(h, self.h_max)

    @staticmethod
    def canonical_line(pt1, pt2):
        """ Ensures line is N->S or W->E, flipping points 'pt1','pt2' if necessary """

        x1, y1 = pt1  # decompose point 'pt1'
        x2, y2 = pt2  # decompose point 'pt2'

        if x1 == x2:
            # vertical line, ensure N->S
            x = x1
            z1 = min(y1, y2)
            z2 = max(y1, y2)
            return (x, z1), (x, z2)
        elif y1 == y2:
            # horizontal line, ensure W->E
            y = y1
            z1 = min(x1, x2)
            z2 = max(x1, x2)
            return (z1, y), (z2, y)
        else:
            raise ValueError("line must be vertical or horizontal: %s->%s" % (str(pt1), str(pt2)))

    @staticmethod
    def intersect(h_line, v_line):
        """ Determine whether the horizontal line 'h_line' intersects the vertical line 'v_line' """

        return (h_line[0][0] <= v_line[0][0] <= h_line[1][0]) and (v_line[0][1] <= h_line[0][1] <= v_line[1][1])

    def __merge_lines(self):
        """ Helper function, core logic in __merge_lines_X functions """
        self.lines["horizontal"] = MondrianPhenotype.__merge_lines_h(self.lines["horizontal"])
        self.lines["vertical"] = MondrianPhenotype.__merge_lines_v(self.lines["vertical"])

    @staticmethod
    def __merge_lines_h(lines):
        """ Merge any connected horizontal lines """
        for line1, line2 in itertools.combinations(lines, 2):  # without replacement
            P1, Q1 = line1
            P2, Q2 = line2
            P1x, P1y = P1
            Q1x, Q1y = Q1
            P2x, P2y = P2
            Q2x, Q2y = Q2

            assert P1x < Q1x, "line not in canonical ordering %u %u" % (P1x, Q1x)
            assert P2x < Q2x, "line not in canonical ordering %u %u" % (P2x, Q2x)

            if P1y == P2y:
                # y-coord are equal, possible merge candidate
                y = P1y

                if P1x <= P2x and Q2x <= Q1x:
                    # P2->Q2 is entirely contained within P1->Q1
                    try:
                        lines.remove(line2)
                    except KeyError:
                        pass
                    return MondrianPhenotype.__merge_lines_h(lines)
                elif P2x <= P1x and Q1x <= Q2x:
                    # P1->Q1 is entirely contained within P2->Q2
                    try:
                        lines.remove(line1)
                    except KeyError:
                        pass
                    return MondrianPhenotype.__merge_lines_h(lines)
                elif Q1x == P2x:
                    # (P1->Q1) -> (P2->Q2) forms a solid line
                    try:
                        lines.remove(line1)
                    except KeyError:
                        pass
                    try:
                        lines.remove(line2)
                    except KeyError:
                        pass
                    lines.add(MondrianPhenotype.canonical_line((P1x, y), (Q2x, y)))
                    return MondrianPhenotype.__merge_lines_h(lines)
                elif P1x == Q2x:
                    # (P2->Q2) -> (P1->Q1) forms a solid line
                    try:
                        lines.remove(line1)
                    except KeyError:
                        pass
                    try:
                        lines.remove(line2)
                    except KeyError:
                        pass
                    lines.add(MondrianPhenotype.canonical_line((P2x, y), (Q1x, y)))
                    return MondrianPhenotype.__merge_lines_h(lines)
                elif P1x <= P2x and P2x <= Q1x:
                    # (P1->Q1) overlaps (P2->Q2) from the left
                    try:
                        lines.remove(line1)
                    except KeyError:
                        pass
                    try:
                        lines.remove(line2)
                    except KeyError:
                        pass
                    lines.add(MondrianPhenotype.canonical_line((P1x, y), (Q2x, y)))
                elif P2x <= P1x and P1x <= Q2x:
                    # (P2->Q2) overlaps (P1->Q1) from the left
                    try:
                        lines.remove(line1)
                    except KeyError:
                        pass
                    try:
                        lines.remove(line2)
                    except KeyError:
                        pass
                    lines.add(MondrianPhenotype.canonical_line((P2x, y), (Q1x, y)))
        return lines

    @staticmethod
    def __merge_lines_v(lines):
        """ Merge any connected vertical lines """
        for line1, line2 in itertools.combinations(lines, 2):  # without replacement
            P1, Q1 = line1
            P2, Q2 = line2
            P1x, P1y = P1
            Q1x, Q1y = Q1
            P2x, P2y = P2
            Q2x, Q2y = Q2

            assert P1y < Q1y, "line not in canonical ordering %u %u" % (P1y, Q1y)
            assert P2y < Q2y, "line not in canonical ordering %u %u" % (P2y, Q2y)

            if P1x == P2x:
                # x-coord are equal, possible merge candidate
                x = P1x

                if P1y <= P2y and Q2y <= Q1y:
                    # P2->Q2 is entirely contained within P1->Q1
                    try:
                        lines.remove(line2)
                    except KeyError:
                        pass
                    return MondrianPhenotype.__merge_lines_v(lines)
                elif P2y <= P1y and Q1y <= Q2y:
                    # P1->Q1 is entirely contained within P2->Q2
                    try:
                        lines.remove(line1)
                    except KeyError:
                        pass
                    return MondrianPhenotype.__merge_lines_v(lines)
                elif Q1y == P2y:
                    # (P1->Q1) -> (P2->Q2) forms a solid line
                    try:
                        lines.remove(line1)
                    except KeyError:
                        pass
                    try:
                        lines.remove(line2)
                    except KeyError:
                        pass
                    lines.add(MondrianPhenotype.canonical_line((x, P1y), (x, Q2y)))
                    return MondrianPhenotype.__merge_lines_v(lines)
                elif P1y == Q2y:
                    # (P2->Q2) -> (P1->Q1) forms a solid line
                    try:
                        lines.remove(line1)
                    except KeyError:
                        pass
                    try:
                        lines.remove(line2)
                    except KeyError:
                        pass
                    lines.add(MondrianPhenotype.canonical_line((x, P2y), (x, Q1y)))
                    return MondrianPhenotype.__merge_lines_v(lines)
                elif P1y <= P2y and P2y <= Q1y:
                    # (P1->Q1) overlaps (P2->Q2) from above
                    try:
                        lines.remove(line1)
                    except KeyError:
                        pass
                    try:
                        lines.remove(line2)
                    except KeyError:
                        pass
                    lines.add(MondrianPhenotype.canonical_line((x, P1y), (x, Q2y)))
                elif P2y <= P1y and P1y <= Q2y:
                    # (P2->Q2) overlaps (P1->Q1) from above
                    try:
                        lines.remove(line1)
                    except KeyError:
                        pass
                    try:
                        lines.remove(line2)
                    except KeyError:
                        pass
                    lines.add(MondrianPhenotype.canonical_line((x, P2y), (x, Q1y)))
        return lines

    rating = property(get_rating, set_rating)


def point_probability_threshold(options, emission_prob):
    """ Derive the probability of emitting a line from an origin point given existing lines

        The argument 'options' is a list of remaining directions in which a line may be drawn. Globally, there are
        probabilities associated with emitting a line based on the remaining options; these probabilities have been
        estimated from true Mondrian artwork.
    """
    if len(options) == 4:
        return emission_prob["Initial"]
    elif len(options) == 3:
        return emission_prob["Terminal"]
    elif len(options) == 2:
        if set(options) == {"North", "South"} or set(options) == {"East", "West"}:
            return emission_prob["OnLine"]  # connected line segments (either horizontal or vertical)
        else:
            return emission_prob["RightAngle"]
    elif len(options) == 1:
        return emission_prob["Nodal"]
    elif len(options) == 0:
        return emission_prob["Cross"]


def genotype_to_phenotype(genotype, phenotype):
    """ Given the genotype, produce a Mondrian phenotype """

    # Produce all possible orderings of the genotype's origin points
    all_orderings = list(itertools.permutations(range(genotype.n_points)))

    # Initialise each point to have all directions possible ...
    avail = {j: MondrianPhenotype.DIRECTIONS[:] for j in range(genotype.n_points)}

    # ... remove directions that take you off the canvas (i.e. where an origin point lies on a boundary)
    for j in range(genotype.n_points):
        if genotype.q[j].allele[0] == 0:
            avail[j].remove("West")
        if genotype.q[j].allele[0] == genotype.width - 1:
            avail[j].remove("East")
        if genotype.q[j].allele[1] == 0:
            avail[j].remove("North")
        if genotype.q[j].allele[1] == genotype.height - 1:
            avail[j].remove("South")

    phenotype.p_val = 1.0 / len(all_orderings)

    # Probabilistically generate lines from origin points applying Mondrian-like rules
    for i in range(genotype.n_loops):
        for j in random.choice(all_orderings):  # randomly select an ordering (i.e. randomise order of traversal)
            threshold = point_probability_threshold(avail[j], MondrianPhenotype.EMISSION_PROB)
            if luck(threshold):
                possible = avail[j]
                direction = genotype.d.random(possible)
                q = tuple(genotype.q[j].allele)
                phenotype.add_random_line(q, direction)
                avail[j].remove(direction)
                phenotype.p_val *= threshold * genotype.d.allele[direction]  # / len(line_ends)
            else:
                phenotype.p_val *= 1.0 - threshold

    # Ensure the result is valid, i.e. eliminate any right-angles
    online_horizontal = lambda options, j: set(options[j]) == {"East", "West"}
    online_vertical = lambda options, j: set(options[j]) == {"North", "South"}
    online = lambda options, j: online_horizontal(options, j) or online_vertical(options, j)
    right_angle = lambda options, j: len(options[j]) == 2 and not online(options, j)
    nodal = lambda options, j: len(options[j]) == 3
    for j in range(genotype.n_points):
        while right_angle(avail, j) or nodal(avail, j):
            direction = genotype.d.random(avail[j])
            phenotype.add_random_line(tuple(genotype.q[j].allele), direction)
            avail[j].remove(direction)
            phenotype.p_val *= genotype.d.allele[direction]

    # Add the origin points - these will only be drawn if requested by the user
    for q_i in genotype.q:
        phenotype.add_point(tuple(q_i.allele))

    # Render the Mondrian-like image (adding coloured rectangles)
    phenotype.finalise()
    phenotype.render()

    return phenotype


def genotype_to_phenotype_enumerate(genotype, phenotype):
    """ Given the genotype, produce all reachable Mondrian phenotypes """

    # Build the command-line argument list to be passed to external (fast) enumerator
    args = []
    args += [os.path.join("genotype-to-phenotype", "genotype_to_phenotype.exe")]
    args += [genotype.model.name, str(phenotype.width), str(phenotype.height), str(phenotype.n_loops)]
    args += ["%u,%u" % tuple(q_i.allele) for q_i in genotype.q]
    args += ["%f,%f,%f,%f" % tuple([genotype.d.allele[direction] for direction in MondrianPhenotype.DIRECTIONS])]

    # Spawn the processing job
    print(" ".join(args))
    external_enumerator = Popen(args, stdout=PIPE)
    all_reachable_phenotype_skeletons = parse_phenotypes(external_enumerator)
    external_enumerator.poll()  # this sets the returncode attribute to detect failures (e.g. out of memory)
    if external_enumerator.returncode is None:
        print("waiting for processing thread to terminate...")
        external_enumerator.wait()
        print("...done")
    if external_enumerator.returncode != 0:
        # A returncode of 0 is SUCCESS, anything else is an ERROR code
        err_msg = "genotype_to_phenotype.exe failed with return code %u" % -external_enumerator.returncode
        err_msg += ", insufficient memory is a likely cause - decrease number of processing cores parameter"
        raise RuntimeError(err_msg)

    # Take the data from the external application and encapsulate as appropriate phenotype objects
    results = {}
    for j, phenotype_skeletons in all_reachable_phenotype_skeletons.items():
        for phenotype_skeleton in phenotype_skeletons:
            possible_phenotype = deepcopy(phenotype)
            possible_phenotype.name += "enumerated"
            possible_phenotype.lines["horizontal"] = phenotype_skeleton["horizontal"]
            possible_phenotype.lines["vertical"] = phenotype_skeleton["vertical"]
            possible_phenotype.p_val = phenotype_skeleton["p_val"]
            for q_i in genotype.q:
                possible_phenotype.add_point(tuple(q_i.allele))
            try:
                results[j].append(possible_phenotype)
            except KeyError:
                results[j] = [possible_phenotype]

    return genotype, results
