from abc import ABCMeta
from template import GAAlgorithm


class Selection(GAAlgorithm):
    """ Template for selection algorithms """

    __metaclass__ = ABCMeta

    def __init__(self, **kwargs):
        """ Keyword argument aware constructor """
        super(Selection, self).__init__(**kwargs)
        self.required_parameters = ["n_parents"]

    def operate(self, individuals, **kwargs):
        """ Minor extension to GAAlgorithm.operate to check post-conditions:
            - the Selection algorithm should produce the correct number of parents
        """
        parents = super(Selection, self).operate(individuals, **kwargs)
        err_msg = "Selection method produced %u parent(s), it should produce %u"
        assert len(parents) == self.n_parents, err_msg % (len(parents), self.n_parents)
        return parents
