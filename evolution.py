from abc import ABCMeta
from template import GAAlgorithm


class Evolution(GAAlgorithm):
    """ Template for overall genetic algorithm (i.e. Evolution) algorithm """

    __metaclass__ = ABCMeta

    def __init__(self, **kwargs):
        """ Keyword argument aware constructor """
        super(Evolution, self).__init__(**kwargs)
        self.required_parameters = ["population_size", "n_elites"]

    def operate(self, individuals, **kwargs):
        """ Minor extension to GAAlgorithm.operate to check post-conditions:
            - the Evolution algorithm should produce the next generation with the correct number of individuals
        """
        next_generation = super(Evolution, self).operate(individuals, **kwargs)
        err_msg = "Evolution method produced %u child/children, it should produce %u"
        assert len(next_generation) == self.population_size, err_msg % (len(next_generation), self.population_size)
        return next_generation
