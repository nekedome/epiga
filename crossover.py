from abc import ABCMeta
from template import GAAlgorithm


class Crossover(GAAlgorithm):
    """ Template for crossover algorithms """

    __metaclass__ = ABCMeta
