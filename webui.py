from implementation.unbiased import UnbiasedDarwindrianPhenotype as MondrianPhenotype
from datetime import datetime
from http.server import BaseHTTPRequestHandler
from urllib import parse
import cgi
import importlib
import logging
import multiprocessing
import os
import random
import re
import traceback


# Named HTTP status codes used in this application
HTTPStatus_OK = 200
HTTPStatus_SEE_OTHER = 303
HTTPStatus_NOT_FOUND = 404


class DarwindrianRequestHandler(BaseHTTPRequestHandler):
    """ Core web server logic for Darwindrian application """

    RESOURCES_PATH = "resources"
    EXPERIMENTS_PATH = "experiments"

    def do_GET(self):
        """ Process a HTTP GET request (static content) """

        logging.info("HTTP GET: %s", self.path)

        # Map various GET requests to their respective handler functions
        resources = os.listdir(self.RESOURCES_PATH)
        handlers = {"/": self.do_GET_main, "/evolve/": self.do_GET_evolve, "/complete/": self.do_GET_complete,
                    "/final/": self.do_GET_final, "/thanks/": self.do_GET_thanks}
        handlers.update({"/%s" % path: self.do_GET_static for path in resources if not path.endswith(".html")})

        # Handle the request
        try:
            self.send_response(HTTPStatus_OK)
            handlers[self.path.rstrip("?")]()  # remove trailing querystring (only if it's empty)
        except KeyError:
            # Perhaps this is a media get request, test for PNG extensions
            if re.match("^/.*\.png$", self.path):  # fixme: more restrictive
                self.send_response(HTTPStatus_OK)
                self.do_GET_static()
            else:
                self.send_error(HTTPStatus_NOT_FOUND, "requested GET resource does not exist")
        except Exception as err:
            self.do_GET_error(err)

    def do_POST(self):
        """ Process a HTTP POST request (form submission) """

        logging.info("HTTP POST: %s", self.path)

        # Map various POST requests to their respective handler functions
        handlers = {"/initialise/": self.do_POST_initialise, "/evolve/": self.do_POST_evolve,
                    "/final/": self.do_POST_final}

        # Handle the request
        try:
            handler = handlers[self.path]
        except KeyError:
            self.send_error(HTTPStatus_NOT_FOUND, "requested POST resource does not exist")
            return
        try:
            handler()
        except Exception as err:  # can any exception thrown from the handler itself
            self.do_GET_error(err)

    def do_GET_main(self):
        """ Handler for 'GET / HTTP/1.1' """

        # Send HTTP headers
        self.send_header("Content-type", "text/html")
        self.end_headers()

        # Send utf-8 encoded HTTP body - recommend holding one core back to run the web server
        html = open(os.path.join(self.RESOURCES_PATH, "index.html"), "r").read()
        html = html.replace("{{DEFAULT_N_CORES}}", "%u" % max(1, multiprocessing.cpu_count() - 1))
        self.wfile.write(bytes(html, "utf8"))

    def do_GET_evolve(self):
        """ Handler for 'GET /evolve/ HTTP/1.1' """

        # Send HTTP headers
        self.send_header("Content-type", "text/html")
        self.end_headers()

        ga = self.server.state["ga"]

        # Generate HTML content - thumbnail pane
        thumbnails = []
        for genotype in ga.population:
            phenotype = genotype.phenotype
            html = "<div class='col-sm-6 col-md-4 col-lg-2'>"
            html += "<img class='img-responsive' onclick=\"togglehidden('preview_%s')\" id='tn_%s' src='/%s/%s.png' />"
            html += "</div>"
            html = html % (phenotype.name, phenotype.name, phenotype.exp_path, phenotype.name)
            padding = " " * 28
            thumbnails.append("%s%s" % (padding, html))

        # Generate HTML content - preview pane and rating forms
        previews = []
        for genotype in ga.population:
            phenotype = genotype.phenotype

            padding = " " * 24
            html = "<div class='preview' id='preview_%s'>" % phenotype.name
            previews.append("%s%s" % (padding, html))

            padding = " " * 28
            html = "<div>"
            previews.append("%s%s" % (padding, html))

            # Full-sized preview of genotype's phenotype, hidden from view
            padding = " " * 32
            html = "<img class='img-responsive' id='preview' src='/%s/%s.png' />" % (phenotype.exp_path, phenotype.name)
            previews.append("%s%s" % (padding, html))

            # Rating radio buttons
            html = "<div class='col-lg-offset-1 col-lg-5'>"
            previews.append("%s%s" % (padding, html))
            html = "<label class='control-label'><h2>%s</h2></label>" % phenotype.RATINGS_LABEL
            padding = " " * 36
            previews.append("%s%s" % (padding, html))
            previews.append("%s<fieldset>" % padding)
            padding = " " * 40
            for rating in genotype.phenotype.RATINGS:
                checked_str = "checked " if rating == genotype.phenotype.DEFAULT_RATING else ""
                html = "<input class='magic-radio' type='radio' name='%s' id='id_%s_%s' value='%s' %s/><label for='id_%s_%s'>%s</label>"
                html = html % (phenotype.name, phenotype.name, rating.name, rating.name, checked_str, phenotype.name, rating.name, rating.name.replace("_", " "))
                previews.append("%s%s" % (padding, html))
            padding = " " * 36
            previews.append("%s</fieldset>" % padding)

            padding = " " * 32
            previews.append("%s</div>" % padding)

            padding = " " * 28
            previews.append("%s</div>" % padding)

            padding = " " * 24
            previews.append("%s</div>" % padding)

        # Send utf-8 encoded HTTP body
        html = open(os.path.join(self.RESOURCES_PATH, "evolve.html"), "r").read()
        html = html.replace("{{THUMBNAILS}}", "\n".join(thumbnails))
        html = html.replace("{{PREVIEWS}}", "\n".join(previews))
        self.wfile.write(bytes(html, "utf8"))

        # Stop the web page from fully loading (although all content has now been delivered) while exploring phenotypes
        ga.epigenetic_search()
        self.server.state["ga"] = ga

    def do_GET_complete(self):
        """ Handler for 'GET /complete/ HTTP/1.1' """

        # Send HTTP headers
        self.send_header("Content-type", "text/html")
        self.end_headers()

        ga = self.server.state["ga"]

        # Parameterise the web page generation
        ga.hyper_parameters["NUMBER_OF_GENERATIONS"] = ga.n_generations
        ga.hyper_parameters["RESULTS_PATH"] = os.path.join(os.getcwd(), ga.exp_path)
        ga.hyper_parameters["PERFORMANCE_GRAPH"] = os.path.join(ga.exp_path, "performance.png")

        # Produce final performance metrics
        fittest = ga.summarise_experiment()

        # Add the fittest individual to the web page generation logic
        ga.hyper_parameters["FITTEST_INDIVIDUAL"] = os.path.join(ga.exp_path, "%s.png" % fittest.phenotype.name)

        # Send utf-8 encoded HTTP body
        html = open(os.path.join(self.RESOURCES_PATH, "complete.html"), "r").read()
        for hyper_parameter, value in ga.hyper_parameters.items():
            html = html.replace("{{%s}}" % hyper_parameter, str(value))  # {{PLACEHOLDER}} template substitution
        self.wfile.write(bytes(html, "utf8"))

    def do_GET_final(self):
        """ Handler for 'GET /final/ HTTP/1.1' """

        # Send HTTP headers
        self.send_header("Content-type", "text/html")
        self.end_headers()

        # Identify candidate experiments - ones that haven't already been finalised
        candidates = []
        last_comparison = {}
        for experiment in os.listdir(self.EXPERIMENTS_PATH):
            # Find candidate experiments
            is_directory = os.path.isdir(os.path.join(self.EXPERIMENTS_PATH, experiment))
            has_meta_rating = os.path.isfile(os.path.join(self.EXPERIMENTS_PATH, experiment, "meta-ratings.txt"))
            if is_directory and not has_meta_rating:
                candidates.append(experiment)
        for candidate in candidates:
            # Extrac the last generation of phenotypes
            files = os.listdir(os.path.join(self.EXPERIMENTS_PATH, candidate))
            images = [file for file in files if re.match("(\d+)_(\d+).png", file)]
            last_comparison[candidate] = sorted(images)[-10:]  # assume generation size is 10...

        # Generate HTML content - thumbnail pane
        thumbnails = []
        for candidate, phenotypes in last_comparison.items():
            for phenotype in phenotypes:
                phenotype_id = "%s_%s" % (candidate, phenotype)
                phenotype_id = phenotype_id.rstrip(".png")
                phenotype_path = os.path.join(self.EXPERIMENTS_PATH, candidate, phenotype)
                html = "<div class='col-sm-6 col-md-4 col-lg-2'>"
                html += "<img class='img-responsive' onclick=\"togglehidden('preview_%s')\" id='tn_%s' src='/%s' />"
                html += "</div>"
                html = html % (phenotype_id, phenotype_id, phenotype_path)
                padding = " " * 28
                thumbnails.append("%s%s" % (padding, html))

        # Generate HTML content - preview pane and rating forms
        previews = []
        for candidate, phenotypes in last_comparison.items():
            for phenotype in phenotypes:
                phenotype_id = "%s_%s" % (candidate, phenotype)
                phenotype_id = phenotype_id.rstrip(".png")
                phenotype_path = os.path.join(self.EXPERIMENTS_PATH, candidate, phenotype)

                preview = []

                padding = " " * 24
                html = "<div class='preview' id='preview_%s'>" % phenotype_id
                preview.append("%s%s" % (padding, html))

                padding = " " * 28
                html = "<div>"
                preview.append("%s%s" % (padding, html))

                # Full-sized preview of genotype's phenotype, hidden from view
                padding = " " * 32
                html = "<img class='img-responsive' id='preview' src='/%s' />" % phenotype_path
                preview.append("%s%s" % (padding, html))

                # Rating radio buttons
                html = "<div class='col-lg-offset-1 col-lg-5'>"
                preview.append("%s%s" % (padding, html))
                html = "<label class='control-label'><h2>%s</h2></label>" % MondrianPhenotype.RATINGS_LABEL
                padding = " " * 36
                preview.append("%s%s" % (padding, html))
                preview.append("%s<fieldset>" % padding)
                padding = " " * 40
                for rating in MondrianPhenotype.RATINGS:
                    checked_str = "checked " if rating == MondrianPhenotype.DEFAULT_RATING else ""
                    html = "<input class='magic-radio' type='radio' name='%s' id='id_%s_%s' value='%s' %s/>"
                    html += "<label for='id_%s_%s'>%s</label>"
                    html = html % (
                    phenotype_id, phenotype_id, rating.name, rating.name, checked_str, phenotype_id,
                    rating.name, rating.name.replace("_", " "))
                    preview.append("%s%s" % (padding, html))
                padding = " " * 36
                preview.append("%s</fieldset>" % padding)

                padding = " " * 32
                preview.append("%s</div>" % padding)

                padding = " " * 28
                preview.append("%s</div>" % padding)

                padding = " " * 24
                preview.append("%s</div>" % padding)

                previews.append("\n".join(preview))

        random.shuffle(thumbnails)  # randomly shuffle the ordering of the images on page

        # Send utf-8 encoded HTTP body
        html = open(os.path.join(self.RESOURCES_PATH, "final.html"), "r").read()
        html = html.replace("{{THUMBNAILS}}", "\n".join(thumbnails))
        html = html.replace("{{PREVIEWS}}", "\n".join(previews))
        self.wfile.write(bytes(html, "utf8"))

    def do_GET_thanks(self):
        """ Handler for 'GET /thanks/ HTTP/1.1' """

        # Send HTTP headers
        self.send_header("Content-type", "text/html")
        self.end_headers()

        # Send utf-8 encoded HTTP body
        html = open(os.path.join(self.RESOURCES_PATH, "thanks.html"), "r").read()
        self.wfile.write(bytes(html, "utf8"))

    def do_GET_static(self):
        """ Handler for 'GET /.*\.(ico|css|js|png) HTTP/1.1' """

        # Send HTTP headers
        extension = self.path.split(".")[-1]  # identify media file extension to set correct HTTP header
        mapping = {"ico": "image/x-icon", "css": "text/css", "js": "text/javascript", "png": "image/png"}
        self.send_header("Content-type", mapping[extension])
        self.end_headers()

        # Identify relative path for this resource and set filename accordingly
        resources = os.listdir(self.RESOURCES_PATH)
        if self.path.lstrip("/") in resources:
            filename = os.path.join(self.RESOURCES_PATH, self.path.lstrip("/"))
        else:
            filename = self.path.lstrip("/")

        # Send utf-8 encoded HTTP body
        try:
            self.wfile.write(open(filename, "rb").read())
        except FileNotFoundError:
            # Somehow we've generated a request for a bad filename, log it and gracefully handle it
            logging.error("bad request for static file: %s", filename)

    def do_GET_error(self, err_msg):
        """ Handler for reporting exceptions back to the user (via HTTP REDIRECT) """

        # Send HTTP headers
        self.send_response(HTTPStatus_OK)
        self.send_header("Content-type", "text/html")
        self.end_headers()

        # Build stack trace to show what code threw an exception/triggered this redirect
        stack_trace = traceback.format_exc()

        # Generate HTML content
        html = open(os.path.join(self.RESOURCES_PATH, "error.html"), "r").read()
        html = html.replace("{{ERROR_MESSAGE}}", "%s: %s" % (type(err_msg).__name__, str(err_msg)))
        html = html.replace("{{STACK_TRACE}}", str(stack_trace))

        # Send utf-8 encoded HTTP body
        self.wfile.write(bytes(html, "utf8"))

    def do_POST_initialise(self):
        """ Handler for 'POST /initialise/ HTTP/1.1' """

        # Extract and pre-process/typecast form variables
        post_vars = self.__extract_post_variables()
        implementation_name = post_vars[b"implementation"][0].decode()
        model = post_vars[b"model"][0].decode()
        weight_k1 = float(post_vars[b"weight_k1"][0].decode())
        weight_k2 = float(post_vars[b"weight_k2"][0].decode())
        n_cores = int(post_vars[b"n_cores"][0].decode())
        width = int(post_vars[b"width"][0])
        height = int(post_vars[b"height"][0])
        n_points = int(post_vars[b"n_points"][0])
        n_loops = int(post_vars[b"n_loops"][0])
        population_size = int(post_vars[b"population_size"][0])
        n_elites = int(post_vars[b"n_elites"][0])
        max_generations = int(post_vars[b"max_generations"][0])
        show_points = b"show_points" in post_vars.keys() and post_vars[b"show_points"][0] == b"on"

        # Experimental setup based on option selected for 'implementation'
        try:
            selected_implementation = importlib.import_module("implementation.%s" % implementation_name)
        except ModuleNotFoundError as err:
            logging.error("must specify valid implementation package - '%s' is invalid", implementation_name)
            self.do_GET_error(err)
            return

        # Define a directory to store results of this experiment, ensure that it doesn't already exist
        exp_path = os.path.join(self.EXPERIMENTS_PATH, datetime.now().strftime("%Y%m%dT%H%M%S"))
        if os.path.exists(exp_path):
            err_msg = "experimental path '%s' already exists" % exp_path
            self.do_GET_error(err_msg)
            return
        os.makedirs(exp_path)
        self.server.state["path"] = exp_path

        # Initialise genetic algorithm experimental context
        ga = selected_implementation.construct_genetic_algorithm(model, width, height, n_points, n_loops, show_points,
                                                                 population_size, n_elites, max_generations, exp_path,
                                                                 weight_k1, weight_k2, n_cores)
        ga.hyper_parameters["IMPLEMENTATION"] = implementation_name
        ga.hyper_parameters["MODEL"] = model
        ga.hyper_parameters["POPULATION_SIZE"] = population_size
        ga.hyper_parameters["NUMBER_OF_ELITES"] = n_elites
        ga.hyper_parameters["MAX_GENERATIONS"] = max_generations
        ga.hyper_parameters["NUMBER_OF_GENERATIONS"] = ga.n_generations
        ga.hyper_parameters["WIDTH"] = width
        ga.hyper_parameters["HEIGHT"] = height
        ga.hyper_parameters["NUMBER_OF_POINTS"] = n_points
        ga.hyper_parameters["NUMBER_OF_LOOPS"] = n_loops
        ga.set_parameter(defer_search=True)

        # Initialise the genetic algorithm
        ga.initialise()
        self.server.state["ga"] = ga

        # Send headers and redirect the web browser
        self.send_response(HTTPStatus_SEE_OTHER)
        self.send_header("Location", "/evolve/")
        self.end_headers()

    def do_POST_evolve(self):
        """ Handler for 'POST /evolve/ HTTP/1.1' """

        # Extract and pre-process/typecast form variables - these contain the rating for each Mondrian
        post_vars = self.__extract_post_variables()

        ga = self.server.state["ga"]

        # Pass Mondrian user ratings back to the underlying phenotypes
        for phenotype, rating_value in post_vars.items():
            rating = rating_value[0].decode()
            for genotype in ga.population:
                if genotype.phenotype.name == phenotype.decode():
                    genotype.phenotype.rating = rating

        # Perform the next step of evolution
        if not ga.terminate():
            ga.iterate()
            self.server.state["ga"] = ga
            self.send_response(HTTPStatus_SEE_OTHER)
            self.send_header("Location", "/evolve/")
            self.end_headers()
        else:
            ga.process_ratings()
            self.server.state["ga"] = ga
            self.send_response(HTTPStatus_SEE_OTHER)
            self.send_header("Location", "/complete/")
            self.end_headers()

    def do_POST_final(self):
        """ Handler for 'POST /final/ HTTP/1.1' """

        # Extract and pre-process/typecast form variables - these contain the rating for each Mondrian
        post_vars = self.__extract_post_variables()

        # Extract final Mondrian user ratings
        meta_ratings = {}
        for phenotype, rating_value in post_vars.items():
            phenotype = phenotype.decode()
            rating = rating_value[0].decode()
            rem = re.match("(?P<experiment>\d+T\d+)_(?P<individual>\d+_\d+)", phenotype)
            if rem is not None:
                experiment, individual = rem.group("experiment"), rem.group("individual")
                if experiment not in meta_ratings.keys():
                    meta_ratings[experiment] = []
                meta_ratings[experiment].append((individual, rating))

        # Map these ratings back to the corresponding experiment - record results
        for experiment, ratings in meta_ratings.items():
            with open(os.path.join(self.EXPERIMENTS_PATH, experiment, "meta-ratings.txt"), "w") as file:
                for individual, rating in ratings:
                    file.write("%s %s\n" % (individual, rating))

        # Redirect to thank-you page
        self.send_response(HTTPStatus_SEE_OTHER)
        self.send_header("Location", "/thanks/")
        self.end_headers()

    def __extract_post_variables(self):
        """ Helper function: extract dictionary encoded HTTP form variables """

        ctype, pdict = cgi.parse_header(self.headers["content-type"])

        if ctype == "multipart/form-data":
            post_vars = cgi.parse_multipart(self.rfile, pdict)
        elif ctype == "application/x-www-form-urlencoded":
            length = int(self.headers["content-length"])
            post_vars = parse.parse_qs(self.rfile.read(length), keep_blank_values=1, strict_parsing=True)
        else:
            post_vars = {}

        return post_vars
