/**
 * Created by Bill on 9/04/2017.
 * Adapted from JSFiddle code snippet - http://jsfiddle.net/XqshE/2/
 */

function togglehidden(id)
{
    if (document.getElementById)
    {
        var divid = document.getElementById(id);
        var divs = document.getElementsByClassName("preview");
        for (var i = 0; i < divs.length; i++)
        {
            divs[i].style.display = "none";
        }
        divid.style.display = "block";
    }
    return false;
}
