from abc import ABCMeta
from template import GAAlgorithm


class Mutation(GAAlgorithm):
    """ Template for mutation algorithms """

    __metaclass__ = ABCMeta
