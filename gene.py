from abc import ABCMeta, abstractmethod
from template import GATemplate
from util import isclose, fitness_proportionate_selection, discretise_integer, gaussian_mutation
import random


class Gene(GATemplate):
    """ Template for genes within a genotype's chromosome """

    __metaclass__ = ABCMeta

    def __init__(self, allele=None, **kwargs):
        """ Keyword argument aware constructor """
        super(Gene, self).__init__(**kwargs)  # process keyword arguments with parent's logic
        self.allele = None
        if allele is not None:
            self.initialise(allele)  # initialise value (allele) of this gene with the provided value
        else:
            self.randomise()  # initialise value (allele) of this gene randomly
        try:
            self.required_parameters += ["allele"]
        except AttributeError:
            self.required_parameters = ["allele"]

    def __repr__(self):
        """ Simple pretty print function showing the value (allele) of this gene """
        return "%s" % self.allele

    def __eq__(self, other):
        """ Instance equality function: test whether self and other have the same allele values """
        if isinstance(other, type(self)):
            return self.allele == other.allele
        else:
            return False

    def __hash__(self):
        """ Hash function on Gene instance definition, allows Gene's to be used as dictionary keys 

            The key data is the value of the Gene's allele - this will be used as input to the hash.
            The allele attribute may be a scalar, a tuple, a list, a dict, etc. Convert any mutable
            allele into an immutable equivalent so that the function hash() can deal with it.
        """
        if isinstance(self.allele, list):
            return hash(tuple(self.allele))
        elif isinstance(self.allele, dict):
            return hash(frozenset(self.allele.items()))
        else:
            return hash(self.allele)

    @abstractmethod
    def initialise(self, allele):
        """ Placeholder for gene initialisation function - to be overridden """
        raise NotImplementedError

    @abstractmethod
    def randomise(self):
        """ Placeholder for gene randomisation function - to be overridden """
        raise NotImplementedError

    @abstractmethod
    def mutate(self):
        """ Placeholder for gene mutation function - to be overridden """
        raise NotImplementedError


class BoundedInt(Gene):
    """ This gene holds an integer that falls in the closed interval [lower, upper] """

    def __init__(self, allele=None, **kwargs):
        try:
            self.required_parameters += ["lower", "upper"]
        except AttributeError:
            self.required_parameters = ["lower", "upper"]
        super(BoundedInt, self).__init__(allele, **kwargs)
        assert isinstance(self.lower, int), "the 'lower' parameter of a BoundedInt must be integer"
        assert isinstance(self.upper, int), "the 'upper' parameter of a BoundedInt must be integer"
        assert self.lower <= self.upper, "the 'lower' and 'upper' parameters of a BoundedInt must be soundly ordered"

    def initialise(self, allele):
        """ Custom initialisation for a BoundedInt """
        invalid_message = "invalid allele value, must be in [%d,%d]" % (self.lower, self.upper)
        if isinstance(allele, int):  # use the int directly
            assert self.lower <= allele <= self.upper, invalid_message
            self.allele = allele
        elif isinstance(allele, BoundedInt):  # extract the int from the BoundedInt
            assert self.lower <= allele.allele <= self.upper, invalid_message
            self.allele = allele.allele
        else:  # no other cases are supported
            raise ValueError("allele must be int or BoundedInt, not %s" % type(allele))

    def randomise(self):
        """ Custom randomisation for a BoundedInt """
        self.allele = random.randint(self.lower, self.upper)  # uniform selection across the closed interval

    def mutate(self):
        """ Custom mutation for a BoundedInt """
        sigma = (self.upper - self.lower + 1) / 3  # 99.7% (3 sigmas) of the probability density covers BoundedInt range
        self.allele = gaussian_mutation(self.allele, lambda allele: self.lower <= allele <= self.upper, sigma, True)


class Bit(BoundedInt):
    """ This gene holds a bit (binary digit) - it is a special case of a BoundedInt """

    def __init__(self, allele=None, **kwargs):
        self.lower = 0
        self.upper = 1
        super(Bit, self).__init__(allele, **kwargs)
        assert self.lower == 0, "the 'lower' parameter of a Bit must be 0"
        assert self.upper == 1, "the 'upper' parameter of a Bit must be 1"


class Point(Gene):
    """ This gene holds a Cartesian point """

    def __init__(self, allele=None, **kwargs):
        try:
            self.required_parameters += ["x_lower", "x_upper", "x_step", "y_lower", "y_upper", "y_step"]
        except AttributeError:
            self.required_parameters = ["x_lower", "x_upper", "x_step", "y_lower", "y_upper", "y_step"]
        super(Point, self).__init__(allele, **kwargs)
        assert isinstance(self.x_lower, int), "the 'x' co-ordinate lower-bound of a Point must be integer"
        assert isinstance(self.x_upper, int), "the 'x' co-ordinate upper-bound of a Point must be integer"
        assert isinstance(self.x_step, int), "the 'x' step size of a Point must be integer"
        assert isinstance(self.y_lower, int), "the 'y' co-ordinate lower-bound of a Point must be integer"
        assert isinstance(self.y_upper, int), "the 'y' co-ordinate upper-bound of a Point must be integer"
        assert isinstance(self.y_step, int), "the 'y' step size of a Point must be integer"
        assert self.x_lower <= self.x_upper, "the (x) 'lower' and 'upper' bounds a Point must be soundly ordered"
        assert self.y_lower <= self.y_upper, "the (y) 'lower' and 'upper' bounds a Point must be soundly ordered"

    def initialise(self, allele):
        """ Custom initialisation of a Point """
        invalid_x_msg = "invalid x co-ordinate, must be in [%d,%d]" % (self.x_lower, self.x_upper)
        invalid_y_msg = "invalid y co-ordinate, must be in [%d,%d]" % (self.y_lower, self.y_upper)
        assert isinstance(allele, list), "allele must be a list of values"
        assert len(allele) == 2, "a Point is two-dimensional, allele must be 2-long"
        assert isinstance(allele[0], int), "a Point is integer valued, x co-ordinate must be an integer"
        assert isinstance(allele[1], int), "a Point is integer valued, y co-ordinate must be an integer"
        assert self.x_lower <= allele[0] <= self.x_upper, invalid_x_msg
        assert self.y_lower <= allele[1] <= self.y_upper, invalid_y_msg
        assert allele[0] % self.x_step == 0, "x co-ordinate must be a multiple of %u" % self.x_step
        assert allele[1] % self.y_step == 0, "y co-ordinate must be a multiple of %u" % self.y_step
        self.allele = allele[:]  # deepcopy the contents of the allele

    def randomise(self):
        """ Custom randomisation for a Point """
        self.allele = [random.randint(self.x_lower, self.x_upper), random.randint(self.y_lower, self.y_upper)]
        self.allele = [discretise_integer(self.allele[0], self.x_step), discretise_integer(self.allele[1], self.y_step)]

    def mutate(self):
        """ Custom mutation for a Point """
        x, y = self.allele
        x_range, y_range = self.x_upper - self.x_lower + 1, self.y_upper - self.y_lower + 1
        sigma_x = x_range / 3  # 99.7% (3 sigmas) of the probability density covers Point range
        sigma_y = y_range / 3  # 99.7% (3 sigmas) of the probability density covers Point range
        self.allele = [gaussian_mutation(x, lambda value: self.x_lower <= value <= self.x_upper, sigma_x, True),
                       gaussian_mutation(y, lambda value: self.y_lower <= value <= self.y_upper, sigma_y, True)]
        self.allele = [discretise_integer(self.allele[0], self.x_step), discretise_integer(self.allele[1], self.y_step)]


class ProbabilityDistribution(Gene):
    """ This gene holds a continuous probability distribution """

    def __init__(self, allele=None, **kwargs):
        try:
            self.required_parameters += ["events"]
        except AttributeError:
            self.required_parameters = ["events"]
        super(ProbabilityDistribution, self).__init__(allele, **kwargs)
        assert isinstance(self.events, list), "'events' must be a list of labeled events that can occur in this P.D.F."

    def initialise(self, allele):
        """ Custom initialisation of a ProbabilityDistribution """
        assert isinstance(allele, dict), "'allele' must be a dictionary mapping events to probabilities"
        err_msg = "expected %u probabilities, got %u" % (len(self.events), len(allele.keys()))
        assert len(allele.keys()) == len(self.events), err_msg
        total_probability = 0.0
        for key, value in allele.items():
            assert key in self.events, "unrecognised event label %s" % key
            assert isinstance(value, float), "probability value must be a floating point"
            total_probability += value
        assert isclose(total_probability, 1.0), "probabilities must sum to 1"
        self.allele = allele

    def randomise(self):
        """ Custom randomisation of a ProbabilityDistribution

            This function implements a uniform sampling of all probability distributions defined on the given discrete
            set of events. This uniform sampling is achieved by selecting a point from a unit simplex in n dimensions
            where n is the number of events in the probability distribution.

            Details and analysis of the algorithm below can be found in the paper:
                Sampling Uniformly from the Unit Simplex - Noah A. Smith and Roy W. Tromble
                http://www.cs.cmu.edu/~nasmith/papers/smith+tromble.tr04.pdf

            Key parameters of this implementation are:
            - n, the number of events in the probability distribution
            - M, the number of discrete probabilities possible (i.e. all probabilities are multiples of 1/M)

            Setting M to a sufficiently large integer will produce sufficient granularity in possible probabilities.

            Finally, this algorithm generates only non-zero probabilities. This limitation is considered acceptable.
        """

        def sample_without_replacement(_X, _M):
            """ Sample from the closed interval [1,M-1] without replacement (X holds selected values so far) """
            if not _X:  # i.e. X is the empty list, just pick any value
                return random.randint(1, _M - 1)  # closed interval [1,M-1]
            x = None
            while x is None or x in _X:
                x = random.randint(1, _M - 1)
            return x

        n = self.n_events
        M = 1.0e9  # all probabilities will be multiples of 1/M or 1.0e-9

        # Generate intermediate x_i from [1,M-1] for i in [1,n-1] (without replacement)
        X = []
        for i in range(n - 1):
            X += [sample_without_replacement(X, M)]

        # Produce the final sorted x_i and extend on each with s.t. x_0 = 0 and x_n = M
        X = [0] + sorted(X) + [M]

        # Reduce n+1 values to n by computing the difference sequence and normalise by M to form probabilities
        Y = [X[i] - X[i-1] for i in range(1, n + 1)]
        Z = [float(y) / M for y in Y]

        # Validate that the probabilities sum to one and assign it to the allele
        assert isclose(sum(Z), 1.0), "random probability distribution must sum to 1 (approximately)"
        self.allele = {event: Z[i] for i, event in enumerate(self.events)}
        assert isclose(sum([self.allele[event] for event in self.events]), 1.0)

    def mutate(self):
        """ Custom mutation of a ProbabilityDistribution """
        event = random.choice(self.events)  # select an event uniformly at random
        sigma = 1.0 / 3.0  # 99.7% (3 sigmas) of the probability density covers a probability range
        self.allele[event] = gaussian_mutation(self.allele[event], lambda value: 0.0 <= value <= 1.0, sigma, False)
        total = sum([self.allele[event] for event in self.events])
        self.allele = {event: self.allele[event] / total for event in self.events}
        assert isclose(sum([self.allele[event] for event in self.events]), 1.0)

    @property
    def n_events(self):
        """ Getter function to calculate the number of events considered by this ProbabilityDistribution """
        return len(self.events)

    def random(self, events=None):
        """ ProbabilityDistribution specific function: randomly pick an event based on the underlying distribution

            Where 'events' is None, draw from the entire distribution.
            Where 'events' is not none, draw from only those events (with rescaled probabilities)
        """

        if events is None:
            return fitness_proportionate_selection(self.allele)  # perform sampling
        else:
            sub_total = sum([self.allele[event] for event in events])  # compute partial total probability
            new_distribution = {event: self.allele[event] / sub_total for event in events}  # rescale
            return fitness_proportionate_selection(new_distribution)  # perform sampling
