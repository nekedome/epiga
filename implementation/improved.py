from evolution import Evolution
from mutation import Mutation
import random
from enum import Enum
from implementation.original import DarwindrianSelection, DarwindrianCrossover, DarwindrianGenotype, DarwindrianPhenotype
from ga import GeneticAlgorithm, Optimisation, Model
from gene import BoundedInt, ProbabilityDistribution, Point
from mondrian import MondrianPhenotype, genotype_to_phenotype
from util import fitness_proportionate_selection, luck
import copy
import math


class ImprovedDarwindrianEvolution(Evolution):
    N_ELITES = 1

    def __init__(self, **kwargs):
        self.n_elites = self.N_ELITES
        super(ImprovedDarwindrianEvolution, self).__init__(**kwargs)
        self.required_parameters += ["selection", "crossover", "mutation"]

    def operate_logic(self, individuals, **kwargs):
        phenotypes = None
        for key, value in kwargs.items():
            if key == "phenotypes":
                phenotypes = value

        next_generation = []
        # add elites
        assert self.n_elites <= self.population_size
        assert self.n_elites == ImprovedDarwindrianEvolution.N_ELITES
        # sort by fitness
        scores = {individual: individual.fitness(phenotypes=phenotypes) for individual in individuals}
        elites = sorted(scores, key=lambda j: scores[j], reverse=True)  # descending order (use self.optimisation)
        elites = elites[:self.n_elites]  # just get the top n (self.n_elites)
        next_generation += [individual.copy_elite() for individual in elites]

        for i in range(self.population_size - 1):
            fitness_ratio = scores[individuals[i]] / scores[individuals[0]]
            if luck(fitness_ratio):
                self.selection.set_parameter(i=i)
                parents = self.selection.operate(individuals)
                offspring = self.crossover.operate(parents)

                # truncate offspring if necessary
                n_needed = self.population_size - len(next_generation)
                offspring = offspring[:n_needed]

                # add them
                next_generation += offspring

        # add the rest (|population| - n_elites)
        while len(next_generation) < self.population_size:
            # perform GA operations
            parents = [copy.deepcopy(fitness_proportionate_selection(scores))]
            next_generation += self.mutation.operate(parents)

        return next_generation


class ImprovedDarwindrianCrossover(DarwindrianCrossover):
    """ Improved Darwindrian crossover

        This is a multi-point crossover algorithm. The genes responsible for:
        - coloured rectangle area thresholds (a_min_width, a_min_height, a_max_width, a_max_height)
        - line direction probability distribution (d)
        - colour selection probability distribution (c)
        - list of generating points (q)
        Are all subject to separate crossover

        The crossover points are determined by the calculate_thresholds and form_new_chromosome functions.
    """

    def operate_logic(self, individuals, **kwargs):
        """ Core implementation of the ImprovedDarwindrianCrossover logic """

        err_msg = "Class %s requires n_parents==2, but n_parents==%u" % (type(self).__name__, len(individuals))
        assert len(individuals) == self.n_parents, err_msg

        g1, g2 = individuals
        x, y, z = self.calculate_thresholds(g1, g2)  # apply problem-specific heuristics

        # Construct a new individual with the same parameters one of the parents
        geno_type = type(g1)
        parameters = g1.parameters
        result = geno_type(**parameters)

        # Set the chromosome of the new individual (offspring) and return it
        result.chromosome = self.form_new_chromosome(g1, g2, x, y, z)
        return [result]  # Crossover algorithms can return many individuals - a list is expected

    @classmethod
    def form_new_chromosome(cls, g1, g2, x, y, z):
        """ Apply the heuristic thresholds x,y,z to crossover chromosomes from g1 and g2 """
        a_min_width, a_min_height, a_max_width, a_max_heigth = cls.crossover_helper_a(g1, g2)
        c_max = cls.crossover_helper_c_max(g1, g2)
        d = cls.crossover_helper_d(g1, g2, x)
        c = cls.crossover_helper_c(g1, g2, z)
        q = cls.crossover_helper_q(g1, g2, y)
        return copy.deepcopy([a_min_width, a_min_height, a_max_width, a_max_heigth, c_max, d, c] + q)

    @classmethod
    def crossover_helper_a(cls, g1, g2):
        """ Apply crossover to area thresholds """

        p1, p2 = g1.phenotype, g2.phenotype

        if p1.structure_rating > p2.structure_rating and len(p1.filled_rectangles) > 0:
            a_min_width = (g1.a_min_width.allele + p1.w_min) // 2
            a_min_height = (g1.a_min_height.allele + p1.h_min) // 2
            a_max_width = (g1.a_max_width.allele + p1.w_max) // 2
            a_max_height = (g1.a_max_height.allele + p1.h_max) // 2
            return BoundedInt(lower=0, upper=g1.width, allele=a_min_width),\
                   BoundedInt(lower=0, upper=g1.height, allele=a_min_height),\
                   BoundedInt(lower=0, upper=g1.width, allele=a_max_width),\
                   BoundedInt(lower=0, upper=g1.height, allele=a_max_height)
        elif p1.structure_rating == p2.structure_rating:
            a_min_width = g1.a_min_width if g1.a_min_width.allele < g2.a_min_width.allele else g2.a_min_width
            a_min_height = g1.a_min_height if g1.a_min_height.allele < g2.a_min_height.allele else g2.a_min_height
            return a_min_width, a_min_height, g1.a_max_width, g2.a_max_height
        elif p1.structure_rating < p2.structure_rating and len(p2.filled_rectangles) > 0:
            a_min_width = (g2.a_min_width.allele + p2.w_min) // 2
            a_min_height = (g2.a_min_height.allele + p2.h_min) // 2
            a_max_width = (g2.a_max_width.allele + p2.w_max) // 2
            a_max_height = (g2.a_max_height.allele + p2.h_max) // 2
            return BoundedInt(lower=0, upper=g1.width, allele=a_min_width), \
                   BoundedInt(lower=0, upper=g1.height, allele=a_min_height), \
                   BoundedInt(lower=0, upper=g1.width, allele=a_max_width), \
                   BoundedInt(lower=0, upper=g1.height, allele=a_max_height)
        elif p1.structure_rating > p2.structure_rating:
            return g1.a_min_width, g1.a_min_height, g1.a_max_width, g1.a_max_width
        elif p1.structure_rating < p2.structure_rating:
            return g2.a_min_width, g2.a_min_height, g2.a_max_width, g2.a_max_width

    @classmethod
    def crossover_helper_c_max(cls, g1, g2):
        """ Apply crossover to maximum number of coloured rectangle threshold """

        p1, p2 = g1.phenotype, g2.phenotype

        if p1.colour_rating > p2.colour_rating:
            c_max = g1.c_max
            if luck(0.5):
                c_max.allele += random.choice([1, -1])
        elif p1.colour_rating == p2.colour_rating:
            c_max_value = (g1.c_max.allele + g2.c_max.allele) // 2
            c_max = BoundedInt(lower=0, upper=g1.MAX_C_MAX, allele=c_max_value)
        elif p1.colour_rating < p2.colour_rating:
            c_max = g2.c_max
            if luck(0.5):
                c_max.allele += random.choice([1, -1])
        return c_max


class ImprovedDarwindrianMutation(Mutation):
    def __init__(self, **kwargs):
        super(ImprovedDarwindrianMutation, self).__init__(**kwargs)

    def operate_logic(self, individuals, **kwargs):
        return individuals


class ImprovedDarwindrianGenotype(DarwindrianGenotype):
    """ Mondrian genotype implementation for the "improved Darwindrian" implementation

        A genotype's chromosome is defined by:
        - a_min_width: the minimum width of a rectangle to colour
        - a_min_height: the minimum height of a rectangle to colour
        - a_max_width: the maximum width of a rectangle to colour
        - a_max_height: the maximum height of a rectangle to colour
        - c_max: the maximum number of rectangles to colour
        - d: the line direction probability distribution
        - c: the colour selection probability distribution
        - {q_i}: a list of generating/origin points

        This class is responsible for managing objects encapsulating this chromosome structure and for expressing
        phenotypes as needed by other genetic operators.
    """

    N_GENES_LESS_POINTS = 7  # number of genes in chromosome (excluding the variable number of generating points)
    MAX_C_MAX = 3  # maximum value for c_max (the maximum number of rectangles to colour when drawing artworks

    def key_fields(self):
        """ The key fields of a MondrianGenotype that determine equivalence

            That is, if two MondrianGenotype instances have matching key fields, they are considered equivalent.
        """
        result = (self.a_min_width, self.a_min_height, self.a_max_width, self.a_max_height, self.c_max, self.d, self.c)
        result += tuple(q_i for q_i in self.q)
        return result

    @property
    def chromosome(self):
        """ Getter for the genotype's chromosome """
        super(ImprovedDarwindrianGenotype, self).chromosome  # assert self.has_value
        result = [self.a_min_width, self.a_min_height, self.a_max_width, self.a_max_height, self.c_max, self.d, self.c]
        result += self.q
        return result

    @chromosome.setter
    def chromosome(self, value):
        """ Setter for the genotype's chromosome """

        # Some basic validation of the provided chromosome
        assert isinstance(value, list), "provided chromosome must be a list, not a(n) %s" % type(value).__name__
        err_msg = "provided chromosome must have length %u, not %u" % (self._n_genes, len(value))
        assert len(value) == self._n_genes, err_msg

        # Utilise the chromosome setter from DarwindrianGenotype - mostly common implementation
        self._n_genes = DarwindrianGenotype.N_GENES_LESS_POINTS + self.n_points
        DarwindrianGenotype.chromosome.fset(self, value[2:4] + value[5:])  # utilise parent's setter
        self._n_genes = ImprovedDarwindrianGenotype.N_GENES_LESS_POINTS + self.n_points
        self.has_value = False

        # Extract and validate the ImprovedDarwindrianGenotype specific genes
        a_min_width = value[0]
        a_min_height = value[1]
        c_max = value[4]
        assert isinstance(a_min_width, BoundedInt)
        assert isinstance(a_min_height, BoundedInt)
        assert isinstance(c_max, BoundedInt)
        assert 0 <= a_min_width.allele <= self.a_max_width.allele <= self.width
        assert 0 <= a_min_height.allele <= self.a_max_height.allele <= self.height
        assert 0 <= c_max.allele <= self.MAX_C_MAX

        # If we got this far, ImprovedDarwindrianGenotype specific genes have been successfully validated - overwrite
        # the current chromosome
        self.a_min_width = a_min_width
        self.a_min_height = a_min_height
        self.c_max = c_max
        self.has_value = True

    def factory(self):
        """ A factory function to generate a random ImprovedDarwindrianGenotype instance

            The current object's hyper-parameters are used to initialise the new object, however, the chromosome of the
            newly created object is entirely random and independent from the current object's chromosome.
        """
        retval = type(self)(**self.parameters)  # create a new object with all of the same parameters as self
        retval.a_min_width = BoundedInt(lower=0, upper=self.width//2)
        retval.a_min_height = BoundedInt(lower=0, upper=self.height//2)
        retval.a_max_width = BoundedInt(lower=self.width//2, upper=self.width)
        retval.a_max_height = BoundedInt(lower=self.height//2, upper=self.height)
        retval.c_max = BoundedInt(lower=0, upper=self.MAX_C_MAX)
        retval.d = ProbabilityDistribution(events=MondrianPhenotype.DIRECTIONS)
        retval.c = ProbabilityDistribution(events=MondrianPhenotype.COLOURS)
        retval.q = None
        x_vals = [self.POINT_DISTANCE_THRESHOLD, self.width - 1 - self.POINT_DISTANCE_THRESHOLD,
                  MondrianPhenotype.STROKE_WIDTH]
        y_vals = [self.POINT_DISTANCE_THRESHOLD, self.height - 1 - self.POINT_DISTANCE_THRESHOLD,
                  MondrianPhenotype.STROKE_WIDTH]
        while retval.q is None or not self.valid_points(retval.q):
            retval.q = [Point(x_lower=x_vals[0], x_upper=x_vals[1], x_step=x_vals[2],
                              y_lower=y_vals[0], y_upper=y_vals[1], y_step=y_vals[2]) for q_i in range(self.n_points)]
        retval.has_value = True
        return retval

    def express(self, name):
        """ Perform phenotype expression """

        # Some basic pre-condition checks
        assert self.has_value, "a genotype cannot express a phenotype without a chromosome set"
        if self.phenotype is not None:
            return  # this genotype has already expressed a phenotype, maintain it

        # Perform the phenotype expression
        attributes = ["a_min_width", "a_min_height", "a_max_width", "a_max_height", "c_max", "c"]
        parameters = self.parameters
        parameters.update({attr: getattr(self, attr) for attr in attributes})
        phenotype = ImprovedDarwindrianPhenotype(name=name, **parameters)
        self.phenotype = genotype_to_phenotype(self, phenotype)


class ImprovedDarwindrianPhenotype(DarwindrianPhenotype):
    """ Mondrian phenotype implementation for the "improved Darwindrian" implementation """

    # Configure the presentation of phenotype rating options in the web interface
    RATINGS = Enum("RATINGS", "like indifferent dislike")
    DEFAULT_RATING = RATINGS.indifferent
    RATINGS_LABEL = "Rating"

    POSITIVE_STRUCTURE_RATING = [RATINGS.like.name]
    POSITIVE_COLOUR_RATING = [RATINGS.like.name]

    MAX_FITNESS = 1.0  # RATINGS.like

    def eligible_rectangle(self, h1, h2, v1, v2):
        """ Criteria that rectangles must satisfy to potentially be coloured
            1. the width of a potential rectangle must lie in [a_min_width, a_max_width]
            2. the height of a potential rectangle must lie in [a_min_height, a_max_height]
        """
        criterion_on_x = self.a_min_width <= math.fabs(v1[0][0] - v2[0][0]) <= self.a_max_width
        criterion_on_y = self.a_min_height <= math.fabs(h1[0][1] - h2[0][1]) <= self.a_max_height
        return criterion_on_x and criterion_on_y

    def fitness(self, **kwargs):
        """ The fitness function on phenotypes """
        rating_map = {self.RATINGS.like: 1.00, self.RATINGS.indifferent: 0.50,
                      self.RATINGS.dislike: 0.10}
        return rating_map[self.RATINGS[self.rating]]


def construct_genetic_algorithm(model, width, height, n_points, n_loops, show_points, population_size, n_elites,
                                max_generations, exp_path, weight_k1, weight_k2, n_cores):
    """ Construct a GeneticAlgorithm instance implementing the "improved Darwindrian" implementation """
    exemplar = ImprovedDarwindrianGenotype(model=Model[model], width=width, height=height, n_points=n_points,
                                           n_loops=n_loops, show_points=show_points, exp_path=exp_path,
                                           weight_k1=weight_k1, weight_k2=weight_k2)
    evolution = ImprovedDarwindrianEvolution(population_size=population_size)
    selection = DarwindrianSelection()
    crossover = ImprovedDarwindrianCrossover()
    mutation = ImprovedDarwindrianMutation()
    ga = GeneticAlgorithm(model=Model[model], optimisation=Optimisation.MAX,
                          max_generations=max_generations)
    ga.set_algorithm(evolution)
    ga.set_algorithm(selection)
    ga.set_algorithm(crossover)
    ga.set_algorithm(mutation)
    ga.set_parameter(exemplar=exemplar)
    ga.set_parameter(exp_path=exp_path)
    ga.set_parameter(n_cores=n_cores)
    return ga
