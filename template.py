from abc import ABCMeta, abstractmethod
import logging


class GATemplate(object):
    """ Class template for various genetic algorithm constructs (e.g. genetic operators, genotypes, etc.) 

        A GATemplate is intended to represent either a complex algorithm (e.g. the genetic algorithm) or a rich object
        (e.g. a Mondrian-like genotype. Thus, a GATemplate is expected to need to have various parameters and/or
        algorithms associated with in. Both of these associations are made with Python attributes; this allows any
        GATemplate instance to readily access their functionality/data.

        To set a parameter, there are two options:
        1. Provide it as a keyword argument to a constructor; for example
            ChildOfGATemplate(parameter=value)
        2. Use the set_parameter function on an object; for example
            x = ChildOfGATemplate()
            x.set_parameter(parameter=value)

        To set an algorithm, there are two options:
        1. Provide it as a keyword argument to a constructor; for example
            ChildOfGATemplate(algorithm_x=algorithm_object)
        2. Use the set_algorithm function on an object; for example
            x = ChildOfGATemplate()
            x.set_algorithm(algorithm_object)

        While similar, a parameter can be set to any object instance while an algorithm must be set to an instance of
        GAAlgorithm. Like GATemplate, GAAlgorithm cannot be instantiated; you must define concrete sub-classes and any
        algorithm must be set to an object of one of these sub-classes.

        This design allows for a hierarchy of algorithms, where each level may utilise arbitrary parameters (some
        optional, some mandatory) as well as other algorithms as building blocks.
    """

    __metaclass__ = ABCMeta  # make abstract, this class cannot be instantiated - it is an interface only

    def __init__(self, **kwargs):
        """ Keyword argument aware constructor """

        # Process all keyword arguments - make them into instance attributes
        self._parameters = {}
        self.set_parameter(**kwargs)

        # Optionally (if self.required_parameters exists) check that all mandatory parameters are present
        if hasattr(self, "required_parameters"):
            self.check_parameters()

    def __repr__(self):
        """ Simple pretty print function to allow the printing of instances """
        return type(self).__name__  # the name of self's class/type

    def set_parameter(self, **kwargs):
        """ Set all keyword arguments to attributes """
        for key, value in kwargs.items():
            if GAAlgorithm.find_algorithm_type(value) is not None:
                self.set_algorithm(value)
            else:
                setattr(self, key, value)  # self.key = value (as an attribute)
                self._parameters[key] = value
            logging.debug("%s.%s=%s", self, key, value)

    def check_parameters(self):
        """ Check that every parameter in self.required_parameters exists as an attribute in self """
        if not hasattr(self, "required_parameters"):
            return  # no mandatory parameters, so check passes
        for parameter in self.required_parameters:
            if not hasattr(self, parameter):
                raise AttributeError("missing mandatory parameter %s" % parameter)

    def set_algorithm(self, *args):
        """ Set all arguments to attributes (if they are instances of GAAlgorithm) """
        for arg in args:
            arg_type = GAAlgorithm.find_algorithm_type(arg)
            if arg_type is not None:
                setattr(self, arg_type.__module__, arg)
                logging.debug("%s.%s=%s (*)", self, arg_type, arg)
            elif arg is not None:
                raise ValueError("unknown algorithm of type %s", type(arg).__name__)

    @property
    def parameters(self):
        return self._parameters


class GAAlgorithm(GATemplate):
    """ Abstract Base Class (ABC) for algorithms """

    __metaclass__ = ABCMeta

    @abstractmethod
    def operate_logic(self, individuals, **kwargs):
        """ Placeholder for algorithm's logic - to be overridden """
        raise NotImplementedError

    def operate(self, individuals, **kwargs):
        """ Ensure mandatory parameters have been set and invoke the algorithm's logic """
        logging.info("%s.operate()", type(self).__name__)
        self.check_parameters()
        return self.operate_logic(individuals, **kwargs)

    @classmethod
    def find_algorithm_type(cls, arg):
        """ Determines whether arg is an instance of GAAlgorithm and returns it's type, or None """
        for valid_algorithm_type in cls.__subclasses__():  # only consider direct descendants
            if isinstance(arg, valid_algorithm_type):
                return valid_algorithm_type
        return None


class GADescription(GATemplate):
    """ Abstract Base Class (ABC) for genetic descriptions (i.e. genotypes and phenotypes) """

    __metaclass__ = ABCMeta

    def __eq__(self, other):
        """ Comparison function between MondrianPhenotypes

            A GADescription instance is considered equivalent to another if certain key fields are equal. What these key
            fields are exactly are deferred to derived classes. This allows for genotype classes to be treated quite
            differently to phenotype classes. It also provides flexibility in different internal representations used by
            each implementation.
        """
        if isinstance(other, type(self)):
            return self.key_fields() == other.key_fields()
        else:
            return False

    def __hash__(self):
        """ When two objects are considered equivalent, their hashes must coincide. As per __eq__, this __hash__
            function assumes that the key_fields() function returns a canonical and ordered collection of the defining
            features of a GADescription instance.

            The generally Python hash() function is then used to hash whatever is returned from key_fields(). This means
            that an immutable collection (such as a tuple or frozenset, but not a list or dict) must be produced.
        """
        return hash(self.key_fields())

    @abstractmethod
    def key_fields(self):
        """ Placeholder function to return the key fields defining a GADescription sub-class - to be overridden """
        raise NotImplementedError

    @abstractmethod
    def fitness(self, **kwargs):
        """ Placeholder for genetic description's fitness function - to be overridden """
        raise NotImplementedError
