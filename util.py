import logging
import math
import random
import re


def isclose(a, b, tolerance=1.0e-6):
    """ Function to test whether a is approximately equal to b (to a given tolerance) """
    return math.fabs(a - b) < tolerance


def fitness_proportionate_selection(scores):
    """ Randomly select an element of scores, with probability proportional to its score

        scores = {event: score}; where
        event is any hashable object and score is a non-negative number

        This is simply the roulette selection algorithm.
    """

    # Calculate total score to allow normalisation
    total_score = sum([scores[event] for event in scores.keys()])

    # Sort the events in descending order, most probable first
    sorted_events = sorted(scores, key=lambda event: scores[event], reverse=True)

    culm_scaled_score = 0.0
    i = 0
    found = False

    # Uniform random selection of r in the closed interval [0, 1]
    r = random.uniform(0.0, 1.0)
    logging.debug("random=%f", r)

    # Find probability bin that catches r and determines selected event
    while i < len(sorted_events) and culm_scaled_score < 1.0:
        culm_scaled_score += scores[sorted_events[i]] / total_score
        if r < culm_scaled_score:
            return sorted_events[i]
        i += 1

    # Edge case where r == 1.0, in this case return the last event
    return sorted_events[-1]  # return last one


def luck(threshold):
    """ Flip a biased coin and return True with probability 'threshold' """
    return random.uniform(0.0, 1.0) < threshold


def discretise_integer(value, step_size):
    """ Adjust the integer 'value' to fit a discretised co-ordinate system specified by 'step_size'

        For example:
        - a 'step_size' of 1 will allow all integer values
        - a 'step_size' of 2 will only allow even integer values
        - a 'step_size' of 3 will only allow integers that are multiples of 3
        - etc.
    """
    return int(step_size * round(float(value) / step_size))


def gaussian_mutation(current_value, is_valid, sigma, is_integer):
    """ Apply Gaussian distributed mutation to 'current_value'

        This function builds a Gaussian distribution with mean 'current_value' and standard deviation 'sigma'. This
        distribution is then sampled to provide the mutated value.

        The function 'is_valid' is applied to validate the new value. If this function evaluated to False on the new
        value the process repeats.

        The 'is_integer' flag indicates whether the new value needs to be rounded to the nearest integer. Rounding is
        performed in this function if 'is_integer' is True.
    """

    # Helper functions to perform appropriate Gaussian sampling - with and without integer rounding
    f_flt = lambda x, _sigma: random.gauss(x, _sigma)
    f_int = lambda x, _sigma: int(round(f_flt(x, _sigma)))
    f_fun = f_int if is_integer else f_flt

    # Sample until we have a new valid value
    new_value = f_fun(current_value, sigma)
    while not is_valid(new_value):
        new_value = f_fun(current_value, sigma)
    return new_value


def parse_phenotypes(external_enumerator):
    """ Parse the output of the external phenotype mapper

        This function processes a stdout pipe to an external application and produces a set of 'phenotype skeletons'.
        A phenotype skeleton is all of the data that defines a phenotype but not encapsulated in a Python object; this
        responsibility lies with the consuming function.
    """

    # Helper functions to parse encoded lines into the underlying co-ordinates
    extract_point = lambda point_str: tuple(map(int, point_str.split(",")))
    extract_line = lambda line_str: tuple(map(extract_point, line_str.split("->")))

    state = None
    p_val = None
    step_size = None
    h_lines, v_lines = set(), set()
    phenotypes = {}

    # Loop over stdout data, one line at a time, until fully consumed
    for line in external_enumerator.stdout:
        line = line.strip().decode()  # strip off new-line and decode into unicode string

        if "P-VALUE" in line:
            try:
                rem = re.match("P-VALUE ([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)", line)
                p_val = float(rem.group(1))
            except (AttributeError, ValueError):
                raise ValueError("genotype_to_phenotype parse error, bad 'p_val' value")
            state = "dummy"  # any non-None value will do to signify a change in the state
        elif "STEP SIZE" in line:
            try:
                rem = re.match("STEP SIZE (\d+)", line)
                step_size = int(rem.group(1))
            except (AttributeError, ValueError):
                raise ValueError("genotype_to_phenotype parse error, bad 'step_size' value")
            state = "s"
        elif line == "HORIZONTAL LINES":
            state = "h"
        elif line == "VERTICAL LINES":
            state = "v"
        elif line == "":
            try:
                phenotypes[step_size].append({"horizontal": h_lines, "vertical": v_lines, "p_val": p_val})
            except KeyError:
                phenotypes[step_size] = [{"horizontal": h_lines, "vertical": v_lines, "p_val": p_val}]
            h_lines, v_lines = set(), set()
            state = None
        elif state == "h":
            h_lines.add(extract_line(line))
        elif state == "v":
            v_lines.add(extract_line(line))
        else:
            raise ValueError("genotype_to_phenotype parse error, unexpected input")

    return phenotypes
