/**
 * Created by Bill on 6/05/2017.
 */

/* After any epi-genetic search is complete, re-enable form buttons */
$(window).load(function(){
    $('input#submitbtn').removeAttr('disabled');
    $('a#completebtn').removeAttr('disabled');
    $('a#restartbtn').removeAttr('disabled');
});

/* Start the main form on index.html with epi-genetic specific fields hidden */
$(window).load(function(){
    $('#weights').hide();
    $('#n_proc_cores').hide();
});

/* On change of genetic algorithm model, re-render epi-genetic specific fields */
$(window).load(function(){
    $('#id_model').change(function(){
        selection = $(this).val();
        switch (selection)
        {
            case 'EGA_EXACT_MATCHES':
                /* The epi-genetic algorithm model with exact matches has one weight */
                $('#weights').show();
                $('#id_weight_k1').show();
                $('#id_weight_k2').hide();
                $('#n_proc_cores').show();
                break;
            case 'EGA_INEXACT_MATCHES':
                /* The epi-genetic algorithm model with exact matches has two weights */
                $('#weights').show();
                $('#id_weight_k1').show();
                $('#id_weight_k2').show();
                $('#n_proc_cores').show();
                break;
            default:
                /* The GA model doesn't need any of these optional fields */
                $('#weights').hide();
                $('#id_weight_k1').hide();
                $('#id_weight_k2').hide();
                $('#n_proc_cores').hide();
                break;
        }
    });
});
