from evolution import Evolution
from selection import Selection
from crossover import Crossover
from genotype import Genotype
from gene import BoundedInt, ProbabilityDistribution, Point
from ga import GeneticAlgorithm, Optimisation, Model
from mondrian import MondrianPhenotype, genotype_to_phenotype
from enum import Enum
from util import luck
import copy
import logging
import math
import random


class DarwindrianEvolution(Evolution):
    """ The general genetic algorithm utilising selection and crossover """

    N_ELITES = 1  # this implementation includes a fixed number of elites

    def __init__(self, **kwargs):
        self.n_elites = DarwindrianEvolution.N_ELITES
        super(DarwindrianEvolution, self).__init__(**kwargs)
        self.required_parameters += ["selection", "crossover"]

    def operate_logic(self, individuals, **kwargs):
        """ Core implementation of the DarwindrianEvolution logic """

        # Attempt to extract phenotypes parameter from keyword arguments
        phenotypes = None
        for key, value in kwargs.items():
            if key == "phenotypes":
                phenotypes = value

        # Start the next generation with the requisite number of elites (i.e. pick individuals with the highest fitness)
        next_generation = []
        assert self.n_elites <= self.population_size
        assert self.n_elites == DarwindrianEvolution.N_ELITES
        scores = {individual: individual.fitness(phenotypes=phenotypes) for individual in individuals}
        elites = sorted(scores, key=lambda j: scores[j], reverse=True)  # descending order (use self.optimisation)
        elites = elites[:self.n_elites]  # just get the top n (self.n_elites)
        next_generation += [individual.copy_elite() for individual in elites]

        # Probabilistically select adjacent individuals as parents
        for i in range(self.population_size - 1):
            fitness_ratio = scores[individuals[i]] / scores[individuals[0]]
            if luck(fitness_ratio):  # select with probability p_i / p_0
                self.selection.set_parameter(i=i)
                parents = self.selection.operate(individuals)
                offspring = self.crossover.operate(parents)

                # Truncate offspring if necessary
                n_needed = self.population_size - len(next_generation)
                offspring = offspring[:n_needed]

                # Include offspring into the next generation
                next_generation += offspring

        # Complete the rest of the next generation
        while len(next_generation) < self.population_size:
            # perform GA operations
            parent = individuals[0].factory()
            next_generation.append(parent)

        return next_generation


class DarwindrianSelection(Selection):
    """ Darwindrian selection

        This is a relatively simple selection mechanism that select the current two adjacent individuals as parents. In
        the calling context, there is an iteration over the current population which sets the current position - the
        member attribute 'i'.
    """

    N_PARENTS = 2

    def __init__(self, **kwargs):
        self.n_parents = DarwindrianSelection.N_PARENTS
        super(DarwindrianSelection, self).__init__(**kwargs)
        self.required_parameters += ["i"]

    def operate_logic(self, individuals, **kwargs):
        """ Core implementation logic of the DarwindrianSelection logic """

        # Attempt to extract phenotypes parameter from keyword arguments
        phenotypes = None
        for key, value in kwargs.items():
            if key == "phenotypes":
                phenotypes = value

        # Select the i^th and (i+1)^th individuals as parents
        scores = {individual: individual.fitness(phenotypes=phenotypes) for individual in individuals}
        sorted_individuals = sorted(scores, key=lambda j: scores[j], reverse=True)
        print(individuals)
        print(self.i, len(individuals), len(scores), self.n_parents, sorted_individuals[self.i:self.i+self.n_parents])
        return sorted_individuals[self.i:self.i+self.n_parents]


class DarwindrianCrossover(Crossover):
    """ Darwindrian crossover

        This is a multi-point crossover algorithm. The genes responsible for:
        - coloured rectangle area thresholds (a_max_width, a_max_height)
        - line direction probability distribution (d)
        - colour selection probability distribution (c)
        - list of generating points (q)
        Are all subject to separate crossover

        The crossover points are determined by the calculate_thresholds and form_new_chromosome functions.
    """

    N_PARENTS = 2

    def __init__(self, **kwargs):
        self.n_parents = DarwindrianCrossover.N_PARENTS
        self.required_parameters = ["n_parents"]
        super(DarwindrianCrossover, self).__init__(**kwargs)

    def operate_logic(self, individuals, **kwargs):
        """ Core implementation of the DarwindrianCrossover logic """

        err_msg = "Class %s requires n_parents==2, but n_parents==%u" % (type(self).__name__, len(individuals))
        assert len(individuals) == self.n_parents, err_msg

        g1, g2 = individuals
        x, y, z = self.calculate_thresholds(g1, g2)  # apply problem-specific heuristics

        # Construct a new individual with the same parameters one of the parents
        geno_type = type(g1)
        parameters = g1.parameters
        result = geno_type(**parameters)

        # Set the chromosome of the new individual (offspring) and return it
        result.chromosome = self.form_new_chromosome(g1, g2, x, y, z)
        return [result]  # Crossover algorithms can return many individuals - a list is expected

    @classmethod
    def calculate_thresholds(cls, g1, g2):
        """ Derive the crossover points for the various genes

            This function is heavily heuristic in nature. Rationale for these particular values should be sought from
            Shen's original work.

            The return values x,y,z are crossover points for:
            x: line direction probability (d)
            y: list of generating points (q)
            z: colour selection probability (c)
        """

        p1, p2 = g1.phenotype, g2.phenotype

        x, y, z = 2, 2, 1
        if p1.structure_rating > p2.structure_rating and p1.colour_rating > p2.colour_rating:
            x, y, z = 4, 3, 3
        elif p1.structure_rating > p2.structure_rating and p1.colour_rating == p2.colour_rating:
            x, y, z = x, 2, random.choice([1, 2])
        elif p1.structure_rating > p2.structure_rating and p1.colour_rating < p2.colour_rating:
            x, y, z = x, 2, 0
        elif p1.structure_rating == p2.structure_rating and p1.colour_rating > p2.colour_rating:
            x, y, z = 2, 2, random.choice([1, 2])
        elif p1.structure_rating == p2.structure_rating and p1.colour_rating == p2.colour_rating:
            x, y, z = 2, random.choice([1, 2]), random.choice([1, 2])
        elif p1.structure_rating == p2.structure_rating and p1.colour_rating < p2.colour_rating:
            x, y, z = 2, random.choice([1, 2]), 0

        return x, y, z

    @classmethod
    def form_new_chromosome(cls, g1, g2, x, y, z):
        """ Apply the heuristic thresholds x,y,z to crossover chromosomes from g1 and g2 """
        a_max_width, a_max_height = cls.crossover_helper_a(g1, g2)
        d = cls.crossover_helper_d(g1, g2, x)
        c = cls.crossover_helper_c(g1, g2, z)
        q = cls.crossover_helper_q(g1, g2, y)
        return copy.deepcopy([a_max_width, a_max_height, d, c] + q)

    @classmethod
    def crossover_helper_a(cls, g1, g2):
        """ Apply crossover to area thresholds """

        p1, p2 = g1.phenotype, g2.phenotype

        if p1.structure_rating > p2.structure_rating:
            return g1.a_max_width, g1.a_max_height
        elif p1.structure_rating == p2.structure_rating:
            new_a_max_width = BoundedInt(lower=g1.a_max_width.lower, upper=g1.a_max_width.upper,
                                         allele=g1.a_max_width.allele)
            new_a_max_height = BoundedInt(lower=g2.a_max_height.lower, upper=g2.a_max_height.upper,
                                          allele=g2.a_max_height.allele)
            return new_a_max_width, new_a_max_height
        elif p1.structure_rating < p2.structure_rating:
            return g2.a_max_width, g2.a_max_height

    @classmethod
    def crossover_helper_d(cls, g1, g2, threshold):
        """ Apply crossover to line direction probability distribution """
        d1, d2 = g1.d.allele, g2.d.allele
        result = {}
        total = 0.0
        for i, direction in enumerate(d1.keys()):
            result[direction] = d1[direction] if i < threshold else d2[direction]
            total += result[direction]
        result = {direction: result[direction] / total for direction in result.keys()}  # normalise
        return ProbabilityDistribution(events=MondrianPhenotype.DIRECTIONS, allele=result)

    @classmethod
    def crossover_helper_c(cls, g1, g2, threshold):
        """ Apply crossover to colour selection probability distribution """
        c1, c2 = g1.c.allele, g2.c.allele
        result = {}
        total = 0.0
        for i, colour in enumerate(c1.keys()):
            result[colour] = c1[colour] if i < threshold else c2[colour]
            total += result[colour]
        result = {colour: result[colour] / total for colour in result.keys()}  # normalise
        print(c1, c1.keys())
        print(c2, c2.keys())
        print(result, result.keys())
        return ProbabilityDistribution(events=MondrianPhenotype.COLOURS, allele=result)

    @classmethod
    def crossover_helper_q(cls, g1, g2, threshold):
        """ Apply crossover to line of generating points """
        return g1.q[:threshold] + g2.q[threshold:]


class DarwindrianGenotype(Genotype):
    """ Mondrian genotype implementation for the "Darwindrian" implementation

        A genotype's chromosome is defined by:
        - a_max_width: the minimum width of a rectangle to colour
        - a_max_height: the minimum height of a rectangle to colour
        - d: the line direction probability distribution
        - c: the colour selection probability distribution
        - {q_i}: a list of generating/origin points

        This class is responsible for managing objects encapsulating this chromosome structure and for expressing
        phenotypes as needed by other genetic operators.
    """

    N_GENES_LESS_POINTS = 4  # number of genes in chromosome (excluding the variable number of generating points)
    C_MAX = 2  # hard-coded maximum number of coloured rectangles

    def __init__(self, **kwargs):
        """ Extract experimental hyper-parameters from the keyword-argument list and validate parameters """
        self.required_parameters = ["width", "height", "n_points", "n_loops", "show_points", "exp_path"]
        self.expressed = False
        super(DarwindrianGenotype, self).__init__(**kwargs)
        assert isinstance(self.width, int)
        assert isinstance(self.height, int)
        assert isinstance(self.n_points, int)
        assert isinstance(self.n_loops, int)
        assert isinstance(self.show_points, bool)
        assert self.width > 0
        assert self.height > 0
        assert self.n_points > 0
        assert self.n_loops > 0
        assert self.exp_path is not None
        self._n_genes = self.N_GENES_LESS_POINTS + self.n_points

    def key_fields(self):
        """ The key fields of a MondrianGenotype that determine equivalence

            That is, if two MondrianGenotype instances have matching key fields, they are considered equivalent.
        """
        return (self.a_max_width, self.a_max_height, self.d, self.c) + tuple(q_i for q_i in self.q)

    @property
    def chromosome(self):
        """ Getter for the genotype's chromosome """
        super(DarwindrianGenotype, self).chromosome  # assert self.has_value
        return [self.a_max_width, self.a_max_height, self.d, self.c] + self.q

    @chromosome.setter
    def chromosome(self, value):
        """ Setter for the genotype's chromosome """

        # Some basic validation of the provided chromosome
        assert isinstance(value, list), "provided chromosome must be a list, not a(n) %s" % type(value).__name__
        err_msg = "provided chromosome must have length %u, not %u" % (self._n_genes, len(value))
        assert len(value) == self._n_genes, err_msg

        # Extract and validate the individual genes
        a_max_width = value[0]
        a_max_height = value[1]
        d = value[2]
        c = value[3]
        q = value[4:4+self.n_points]
        assert isinstance(a_max_width, BoundedInt)
        assert isinstance(a_max_height, BoundedInt)
        assert isinstance(d, ProbabilityDistribution)
        assert isinstance(c, ProbabilityDistribution)
        assert isinstance(q, list)
        assert len(q) == self.n_points
        for q_i in q:
            assert isinstance(q_i, Point)
            assert q_i.x_lower == self.POINT_DISTANCE_THRESHOLD
            assert q_i.x_upper == self.width - 1 - self.POINT_DISTANCE_THRESHOLD
            assert q_i.x_step == MondrianPhenotype.STROKE_WIDTH
            assert q_i.y_lower == self.POINT_DISTANCE_THRESHOLD
            assert q_i.y_upper == self.height - 1 - self.POINT_DISTANCE_THRESHOLD
            assert q_i.y_step == MondrianPhenotype.STROKE_WIDTH
        assert 0 <= a_max_width.allele <= self.width
        assert 0 <= a_max_height.allele <= self.height

        # If we got this far, all genes have been successfully validated - overwrite the current chromosome
        self.a_max_width = BoundedInt(lower=0, upper=self.width, allele=self.width)
        self.a_max_height = BoundedInt(lower=0, upper=self.height, allele=self.height)
        self.d = d
        self.c = c
        self.q = q
        self.has_value = True

    def factory(self):
        """ A factory function to generate a random DarwindrianGenotype instance

            The current object's hyper-parameters are used to initialise the new object, however, the chromosome of the
            newly created object is entirely random and independent from the current object's chromosome.
        """
        retval = type(self)(**self.parameters)  # create a new object with all of the same parameters as self
        retval.a_max_width = BoundedInt(lower=0, upper=self.width, allele=self.width)
        retval.a_max_height = BoundedInt(lower=0, upper=self.height, allele=self.height)
        retval.d = ProbabilityDistribution(events=MondrianPhenotype.DIRECTIONS)
        retval.c = ProbabilityDistribution(events=MondrianPhenotype.COLOURS)
        retval.q = None
        x_vals = [self.POINT_DISTANCE_THRESHOLD, self.width - 1 - self.POINT_DISTANCE_THRESHOLD,
                  MondrianPhenotype.STROKE_WIDTH]
        y_vals = [self.POINT_DISTANCE_THRESHOLD, self.height - 1 - self.POINT_DISTANCE_THRESHOLD,
                  MondrianPhenotype.STROKE_WIDTH]
        while retval.q is None or not self.valid_points(retval.q):
            retval.q = [Point(x_lower=x_vals[0], x_upper=x_vals[1], x_step=x_vals[2],
                              y_lower=y_vals[0], y_upper=y_vals[1], y_step=y_vals[2]) for q_i in range(self.n_points)]
        retval.has_value = True
        return retval

    def express(self, name):
        """ Perform phenotype expression """

        # Some basic pre-condition checks
        assert self.has_value, "a genotype cannot express a phenotype without a chromosome set"
        if self.phenotype is not None:
            return  # this genotype has already expressed a phenotype, maintain it

        # Perform the phenotype expression
        parameters = self.parameters
        parameters.update({attr: getattr(self, attr) for attr in ["a_max_width", "a_max_height", "c"]})
        parameters.update({"c_max": self.C_MAX})
        phenotype = DarwindrianPhenotype(name=name, **parameters)
        self.phenotype = genotype_to_phenotype(self, phenotype)


class DarwindrianPhenotype(MondrianPhenotype):
    """ Mondrian phenotype implementation for the "Darwindrian" implementation """

    # Configure the presentation of phenotype rating options in the web interface
    RATINGS = Enum("RATINGS", "both structure colour none")
    DEFAULT_RATING = RATINGS.none
    RATINGS_LABEL = "Things I like"

    POSITIVE_STRUCTURE_RATING = [RATINGS.both.name, RATINGS.structure.name]
    POSITIVE_COLOUR_RATING = [RATINGS.both.name, RATINGS.colour.name]

    MAX_FITNESS = 2.0  # RATINGS.both

    def eligible_rectangle(self, h1, h2, v1, v2):
        """ Criteria that rectangles must satisfy to potentially be coloured
            1. the width of a potential rectangle must lie in [0, a_max_width]
            2. the height of a potential rectangle must lie in [0, a_max_height]
        """
        criterion_on_x = math.fabs(v1[0][0] - v2[0][0]) <= self.a_max_width
        criterion_on_y = math.fabs(h1[0][1] - h2[0][1]) <= self.a_max_height
        return criterion_on_x and criterion_on_y

    def fitness(self, **kwargs):
        """ The fitness function on phenotypes """
        rating_map = {self.RATINGS.both: 1.00, self.RATINGS.structure: 0.75, self.RATINGS.colour: 0.65,
                      self.RATINGS.none: 0.30}
        return rating_map[self.RATINGS[self.rating]]


def construct_genetic_algorithm(model, width, height, n_points, n_loops, show_points, population_size, n_elites,
                                max_generations, exp_path, weight_k1, weight_k2, n_cores):
    """ Construct a GeneticAlgorithm instance implementing the "Darwindrian" implementation """
    exemplar = DarwindrianGenotype(model=Model[model], width=width, height=height, n_points=n_points, n_loops=n_loops,
                                   show_points=show_points, exp_path=exp_path, weight_k1=weight_k1, weight_k2=weight_k2)
    evolution = DarwindrianEvolution(population_size=population_size)
    selection = DarwindrianSelection()
    crossover = DarwindrianCrossover()
    ga = GeneticAlgorithm(model=Model[model], optimisation=Optimisation.MAX,
                          max_generations=max_generations)
    ga.set_algorithm(evolution)
    ga.set_algorithm(selection)
    ga.set_algorithm(crossover)
    ga.set_parameter(exemplar=exemplar)
    ga.set_parameter(exp_path=exp_path)
    ga.set_parameter(n_cores=n_cores)
    return ga
