from enum import Enum
from mondrian import genotype_to_phenotype_enumerate
from template import GATemplate, GADescription
import csv
import gzip
import logging
try:
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
except ImportError:
    plt = None  # flag the failure so no plotting is attempted
import multiprocessing
import os
import pickle


Optimisation = Enum("Optimisation", "MIN MAX")  # type of optimisation to perform
Model = Enum("Model", "GA EGA_EXACT_MATCHES EGA_INEXACT_MATCHES")  # which GA model to utilise


class GeneticAlgorithm(GATemplate):
    """ General implementation of the genetic algorithm

        This class utilises functions/interfaces provided by abstract base classes representing various genetic
        operators (e.g. evolution, selection, crossover, mutation). The details of these genetic operators are not
        contained within this class, such details are contained in specific GeneticAlgorithm "implementations". These
        can be found in the implementation sub-module.

        The specific genetic operators should be set with the GATemplate.set_algorithm() function. For example:
            ga = GeneticAlgorithm(model=X, optimisation=Y, max_generations=Z)
            ga.set_algorithm(Evolution())
            ga.set_algorithm(Selection())
            ga.set_algorithm(Crossover())
            ga.set_algorithm(Mutation())
    """

    def __init__(self, **kwargs):
        """ GeneticAlgorithm constructor """

        # Extract and validate experimental hyper-parameters from the keyword arguments
        self.required_parameters = ["model", "optimisation", "max_generations"]
        super(GeneticAlgorithm, self).__init__(**kwargs)
        self.hyper_parameters = {}  # convenience dictionary used for reporting

        # The specific genetic operator/policies to use as needed
        self.evolution = None
        self.selection = None
        self.crossover = None
        self.mutation = None

        # Internal state of the genetic algorithm as it executes
        self.population = None
        self.n_generations = None
        self.exemplar = None  # used to produce genotypes of the correct type

        # By default, do not explicitly perform epi-genetic inference. This allows an explicit call to initiate such
        # computationally expensive work at the right time based on the details of the consuming application. For
        # example, it may make sense to begin this work before or after certain web pages have loaded in the Darwindrian
        # web interface.
        self.defer_search = False

        # Observed and manually rated phenotypes are accumulated overtime to allow for the calculation of genotype
        # fitness values - these fitness functions are parameterised by this set at any point in time
        self.phenotypes = []

        # To store basic metrics collected over the course of an experiment
        self.metrics = {}

    def optimise(self):
        """ The core genetic algorithm, run through until a termination condition is encountered """

        self.initialise()
        while not self.terminate():
            self.iterate()

        # Return fittest genotype
        return self.fittest()

    def initialise(self):
        """ Initialise the genetic operators and produce the first population """

        logging.info("%s.initialise()", type(self).__name__)

        # Check pre-conditions
        assert self.evolution is not None, "the genetic algorithm requires an Evolution algorithm"
        assert isinstance(self.exemplar, GADescription), "the exemplar genotype must be a GADescription instance"

        self.evolution.set_algorithm(self.selection)
        self.evolution.set_algorithm(self.crossover)
        self.evolution.set_algorithm(self.mutation)

        self.population = [self.exemplar.factory() for _ in range(self.evolution.population_size)]
        self.n_generations = 1

        self.express_phenotypes()  # express phenotypes and optionally perform epi-genetic inference

    def terminate(self):
        """ Simple terminating condition based on a maximum number of generations """
        return self.n_generations >= self.max_generations

    def iterate(self):
        """ Step the genetic algorithm by one generation """

        logging.info("%s.iterate() - n_generations == %u", type(self).__name__, self.n_generations)

        self.process_ratings()

        # Propagate to the next generation
        self.population = self.evolution.operate(self.population, phenotypes=self.phenotypes)
        self.n_generations += 1

        self.express_phenotypes()  # express phenotypes and optionally perform epi-genetic inference

    def process_ratings(self):
        # Capture key metrics from the current population before moving on
        local_metrics = []
        for individual in self.population:
            phenotype = individual.phenotype
            geno_fitness = individual.fitness(phenotypes=self.phenotypes)
            temp = [individual.n_epiga_hits, phenotype.p_val, phenotype.rating, phenotype.fitness(), geno_fitness]
            local_metrics.append((individual, temp))
        self.metrics[self.n_generations] = local_metrics

    def fittest(self):
        """ Identify the fittest individual encountered during the genetic algorithm so far

            This individual will lie in the current generation if elite selection is employed. However, since this is
            optional, the entire history is considered when identifying the fittest.
        """
        best = None
        for individual in self.population:
            if best is None or individual.fitness(phenotypes=self.phenotypes) > best.fitness(phenotypes=self.phenotypes):
                best = individual
        return best

    def express_phenotypes(self):
        """ Express a phenotype from each genotype in the current generation, optionally perform epi-genetic inference

            The epi-genetic inference only occurs if self.defer_search == False and if the genetic algorithm model
            requires it; that is, model is one of the epi-genetic models.
        """

        for i, genotype in enumerate(self.population):
            genotype.express("%u_%u" % (self.n_generations, i))  # simple generation#_genotype_# naming convention
            self.phenotypes.append(genotype.phenotype)  # accumulate all newly expressed phenotypes

        if not self.defer_search:
            self.epigenetic_search()

    def epigenetic_search(self):
        """ If required by the genetic algorithm model, perform epi-genetic inference

            The epi-genetic inference pre-computes the collection of all phenotypes reachable by the genotypes in the
            current generation. This computationally expensive search is off-loaded to a native implementation of the
            algorithm via Python pipes.

            The actual use of these phenotypes lists is deferred to genotype fitness functions, however, at that point
            it can be assumed that this list already exists.
        """

        if self.model in [Model.EGA_EXACT_MATCHES, Model.EGA_INEXACT_MATCHES]:
            # Build the command-line arguments to be passed to the native-code search implementation
            args = [(individual, individual.phenotype) for individual in self.population]

            # Create a pool of worker processes to share the computational work - the number of workers is provided by
            # the user and will each consume a CPU core. Distribute the work requests to the work pool.
            pool = multiprocessing.Pool(self.n_cores)
            async_results = [pool.apply_async(genotype_to_phenotype_enumerate, arg) for arg in args]
            pool.close()
            pool.join()

            # Parse the results from each of the job requests
            for result in async_results:
                result.wait()  # wait/join on the worker thread
                genotype, phenotypes = result.get()  # get the results from the worker's stdout
                for individual in self.population:
                    if individual == genotype:  # find the initial genotype and attach all reachable phenotypes
                        individual.reachable_phenotypes = phenotypes

    def summarise_experiment(self):
        """ Derive summary statistics and performance measures from the experiment

            Some of these statistics/measures are assigned to member attributes while others are exported as files
            (serialised objects, graphs, etc.). The following summaries are produced:
            - Every produced genotype is serialised (with identifying information about its generation)
            - Every expressed phenotype is serialised (clearly linked to the corresponding genotype)
            - Every expressed phenotype is rendered as a PNG (performed during the experiment)
            - Key metrics are tracked for every genotype/expressed phenotype, output as a CSV file:
              -
            - Key performance measures are plotted as images

            All of this data is created in the experimental sub-directory created for this experiment.
        """

        # Perform serialisation
        rating_sequence = []
        fitness_sequence = []
        fittest, fitness_value = None, None
        setup_path = os.path.join(self.exp_path, "set-up.txt")
        csv_path = os.path.join(self.exp_path, "metrics.csv")
        try:
            with open(setup_path, "w", newline="") as file:
                for hyper_parameter, setting in self.hyper_parameters.items():
                    file.write("%s=%s\n" % (hyper_parameter, setting))
            with open(csv_path, "w", newline="") as file:
                metric_writer = csv.writer(file, quoting=csv.QUOTE_MINIMAL)
                for generation, all_metrics in self.metrics.items():
                    rating, fitness = 0.0, 0.0
                    for genotype, metrics in all_metrics:
                        name = genotype.phenotype.name
                        genotype_path = os.path.join(self.exp_path, "%s.genotype" % name)
                        phenotype_path = os.path.join(self.exp_path, "%s.phenotype" % name)

                        # Serialise the genotype and phenotype actually expressed
                        with gzip.GzipFile(genotype_path + ".gz", "wb") as gz_file:
                            pickle.dump(genotype, gz_file)
                        with gzip.GzipFile(phenotype_path + ".gz", "wb") as gz_file:
                            pickle.dump(genotype.phenotype, gz_file)

                        metric_writer.writerow(["%u" % generation, name] + metrics)

                        # Write key metrics out to a CSV file and accumulate key performance measures
                        rating += metrics[3]
                        fitness += metrics[4]
                        if fittest is None or metrics[4] > fitness_value:
                            fittest = genotype
                            fitness_value = metrics[4]
                    rating_sequence.append(float(rating) / self.evolution.population_size)
                    fitness_sequence.append(float(fitness) / self.evolution.population_size)
        except Exception as err:
            print("Exception thrown during serialisation")
            print(err)

        if plt is None:
            # The matplotlib library isn't installed - do not attempt to produce performance graph
            return fittest

        # Plot performance graph
        try:
            plt.close()  # necessary to avoid error if this is not the first graph drawn
            fig, ax1 = plt.subplots()
            ax1.plot(rating_sequence, "b")
            ax2 = ax1.twinx()
            ax2.plot(fitness_sequence, "r")
            fig.tight_layout()
            plt.savefig(os.path.join(self.exp_path, "performance.png"))
        except Exception as err:
            print("Exception thrown during plotting")
            print(err)
        return fittest
